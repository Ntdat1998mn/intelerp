export const danhSachLuong = [
    {
        bangluong_id: 1,
        bangluong_name: "Ngọc Loan",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Ngọc Thoa",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Mỹ Linh",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Ngọc Mai",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Kỳ Duyên",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Triệu Vy",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    },
    {
        bangluong_id: 1,
        bangluong_name: "Ngọc Thương",
        bangluong_lcb: 25000,
        bangluong_gioCong: 8,
        bangluong_pctn: 0,
        bangluong_pcxang: 0,
        bangluong_pccom: 0,
        bangluong_pcdt: 0,
        bangluong_pcthemGio: 0,
    }
]
export const noiQuy = [
    {
        noiquy_id: 1,
        noiquy_name: "Nội Quy 1",
        noiquy_price: "2000000"
    },
    {
        noiquy_id: 2,
        noiquy_name: "Nội Quy 2",
        noiquy_price: "1000000"
    }
]
export const danhSachViPham = [
    {
        vipham_id: 1,
        vipham_name: "Hồ Việt Uyên",
        viphamn_noiQuy: 1,
        vipham_soTien: "100000"
    },
    {
        vipham_id: 2,
        vipham_name: "Hồ Việt Uyên",
        viphamn_noiQuy: 2,
        vipham_soTien: "200000"
    },
    {
        vipham_id: 3,
        vipham_name: "Nguyễn Đình Tuấn",
        viphamn_noiQuy: 1,
        vipham_soTien: "100000"
    },
]
export const khenThuong = [
    {
        noiquy_id: 1,
        noiquy_name: "Chuyên Cần",
        noiquy_price: "2000000"
    },
    {
        noiquy_id: 2,
        noiquy_name: "Vượt KPI",
        noiquy_price: "1000000"
    }
]
export const danhSachKhenThuong = [
    {
        khenthuong_id: 1,
        khenthuong_name: "Hồ Việt Uyên",
        khenthuong_noiQuy: 1,
        khenthuong_soTien: "100000"
    },
    {
        khenthuong_id: 2,
        khenthuong_name: "Hồ Việt Uyên",
        khenthuong_noiQuy: 2,
        khenthuong_soTien: "200000"
    },
    {
        khenthuong_id: 3,
        khenthuong_name: "Nguyễn Đình Tuấn",
        khenthuong_noiQuy: 1,
        khenthuong_soTien: "100000"
    },
]
export const ungLuong = [
    {
        ungluong_id: 1,
        ungluong_nhanvien: "Ngô Minh Triều",
        ungluong_ghichu: "Ứng Lương",
        ungluong_tien: 500000
    },
    {
        ungluong_id: 2,
        ungluong_nhanvien: "Huỳnh Minh Mẫn",
        ungluong_ghichu: "Ứng Lương",
        ungluong_tien: 100000
    },
    {
        ungluong_id: 3,
        ungluong_nhanvien: "Nguyễn Tiến Đạt",
        ungluong_ghichu: "Ứng Lương",
        ungluong_tien: 1000000
    }
]
export const tangGiamLuong =  [
    {
        tanggiam_id : 1,
        tanggiam_name: "Nguyễn Văn A",
        tanggiam_lcb: 5000000,
        tanggiam_tang: 1000000,
        tanggiam_giam: 0,
        tanggiam_ghichu: "Hoàn Thành Thử Việc"
    },
    {
        tanggiam_id : 2,
        tanggiam_name: "Nguyễn Văn B",
        tanggiam_lcb: 5000000,
        tanggiam_tang: 0,
        tanggiam_giam: 500000,
        tanggiam_ghichu: "Giảm trợ cấp"
    }
]