import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const folderService = {
  layItemList: (token) => {
    return axios({
      url: BASE_URL + `/api/quanly_tailieu/items/lay-item-list`,
      method: "GET",
      headers: { token: token },
    });
  },
  doiTenThuMuc: (id, data, token) => {
    return axios({
      url: BASE_URL + `/api/quanly_tailieu/items/doi-ten-thu-muc/${id}`,
      method: "PUT",
      data: data,
      headers: { token: token },
    });
  },
};
