import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const userService = {
  login: (userForm) => {
    return axios({
      url: BASE_URL + "/api/quanly_tailieu/auth/dang-nhap",
      method: "POST",
      data: userForm,
    });
  },
  uploadAvatar: (data, token) => {
    const formData = new FormData();
    formData.append("avatar", data);
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/nguoi-dung/upload-avatar",
      method: "POST",
      data: formData,
      headers: { token: token },
    });
  },
  updateUserInfor: (data, token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/nguoi-dung/cap-nhat-thong-tin-nhan-vien",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
  /*   getDirectorateUserList: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/nguoi-dung/lay-danh-sach-nguoi-dung-ban-giam-doc",
      method: "GET",
      headers: { token: token },
    });
  }, */
  layNguoiDungCungPhongBan: (id, token) => {
    return axios({
      url:
        BASE_URL +
        `/api/dinh-vi-cham-cong/nguoi-dung/lay-danh-sach-nguoi-dung-theo-phong-ban/${id}`,
      method: "GET",
      headers: { token: token },
    });
  },
  layNguoiDungBanLanhDaoCongTy: (token) => {
    return axios({
      url:
        BASE_URL +
        `/api/dinh-vi-cham-cong/nguoi-dung/lay-danh-sach-nguoi-dung-ban-lanh-dao-cong-ty`,
      method: "GET",
      headers: { token: token },
    });
  },
};
