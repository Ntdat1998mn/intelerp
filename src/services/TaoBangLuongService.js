export const bangThongSo = [
    {
        thongSo_id: 1,
        thongSo_name: "Lương Cơ Bản",
        thongSo_kyHieu: "LCB"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Tăng/Giảm Lương",
        thongSo_kyHieu: "TGL"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Lương Chính Thức (LCB + TGL)",
        thongSo_kyHieu: "LCT"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Ứng Lương",
        thongSo_kyHieu: "UL"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Lương 1 Ngày Công",
        thongSo_kyHieu: "LNC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Lương 1 Giờ Công",
        thongSo_kyHieu: "LGC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Giờ Công",
        thongSo_kyHieu: "GC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Giờ Tăng Ca",
        thongSo_kyHieu: "GTC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Ngày Làm Việc",
        thongSo_kyHieu: "GTC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Nghỉ Có Phép",
        thongSo_kyHieu: "NCP"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Nghỉ Không Phép",
        thongSo_kyHieu: "NKP"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Nghỉ Lễ",
        thongSo_kyHieu: "NL"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Số Ngày Trong Tháng",
        thongSo_kyHieu: "TDAY"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Thưởng",
        thongSo_kyHieu: "THG"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phạt",
        thongSo_kyHieu: "PHT"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Trách Nhiệm",
        thongSo_kyHieu: "PCTN"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Tiền Xăng",
        thongSo_kyHieu: "PCTX"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Điện Thoại",
        thongSo_kyHieu: "PCDT"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Thêm Giờ",
        thongSo_kyHieu: "PCTG"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Tiền Cơm",
        thongSo_kyHieu: "PCTC"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Phụ Cấp Chuyên Cần",
        thongSo_kyHieu: "PCCN"
    },
    {
        thongSo_id: 1,
        thongSo_name: "Bảo Hiểm Xã Hội",
        thongSo_kyHieu: "BHXH"
    }
]
export const taoCongThuc = [
    {
        bangLuong_id: 1,
        bangLuong_name:"Lương Ngày Thường",
        bangLuong_status: 0,
        bangLuong_type: 0,
        bangLuong_number: 300000,
        bangLuong_congThuc : "@LCB@*@NLV@",
        bangLuong_all: 0
    },
    {
        bangLuong_id: 1,
        bangLuong_name:"Lương Ngày Lễ",
        bangLuong_status: 1,
        bangLuong_type: 1,
        bangLuong_number: 300000,
        bangLuong_congThuc : "@LCB@*@NLV@",
        bangLuong_all: 0
    }
]
export const cauHinh = [
    {
        danhMuc_id : 1,
        danhMuc_name: "BHXH",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Ký Quỹ",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Công Nợ",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Kỷ Luật",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Đào Tạo",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Hồ Sơ",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Nhắc Nhở",
        danhMuc_status: 1
    },
    {
        danhMuc_id : 1,
        danhMuc_name: "Máy Chấm Công",
        danhMuc_status: 1
    }
]
export const thoiGianCaLamViec = {
    hanhChinh:{
        gioVao: "06:00",
        gioRa: "17:00"
    },
    tangCa:{
        gioVao: "18:00",
        gioRa: "21:00"
    }
}
export const gioCongChuan = {
    hanhChinh : 8,
    tangCa: 3
}
export const thoiGianParttime = {
    ca1: {
        gioVao:"06:00",
        gioRa: "12:00"
    },
    ca2: {
        gioVao:"13:00",
        gioRa: "17:00"
    },
    ca3: {
        gioVao:"18:00",
        gioRa: "21:00"
    }
}
export const gioCongChuanParttime = {
    ca1: 4,
    ca2: 4,
    ca3: 3
}