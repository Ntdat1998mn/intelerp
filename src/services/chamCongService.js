export const  nhanSu = [
    {
        phongBan_id: 1,
        phongBan_name: "Ban Giám Đốc",
        nhanVien: [
            {
                nhanvien_id: 1,
                nhanvien_name: "Nguyễn Văn A"
            },
            {
                nhanvien_id: 2,
                nhanvien_name: "Nguyễn Thị B"
            },
            {
                nhanvien_id: 3,
                nhanvien_name: "Nguyễn Văn C"
            }
        ]
    },{
        phongBan_id: 2,
        phongBan_name: "Ban Quản Lý",
        nhanVien: [
            {
                nhanvien_id: 4,
                nhanvien_name: "Trần A"
            },
            {
                nhanvien_id: 5,
                nhanvien_name: "Trần B"
            },
            {
                nhanvien_id: 6,
                nhanvien_name: "Trần C"
            }
        ]
    },{
        phongBan_id: 3,
        phongBan_name: "Phòng Nhân Sự",
        nhanVien: [
            {
                nhanvien_id: 7,
                nhanvien_name: "Đinh Văn A"
            },
            {
                nhanvien_id: 8,
                nhanvien_name: "Đinh Thị B"
            },
            {
                nhanvien_id: 9,
                nhanvien_name: "Đinh Văn C"
            }
        ]
    }
]
export const chamTay = [
    {
        user_id: 1,
        user_name: "Nguyễn Văn A",
        bangCong: [
            {
                work: "1",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },
    {
        user_id: 2,
        user_name: "Nguyễn Văn B",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },
    {
        user_id: 3,
        user_name: "Nguyễn Văn C",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },
    {
        user_id: 4,
        user_name: "Nguyễn Văn D",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },
    {
        user_id: 5,
        user_name: "Nguyễn Văn E",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },{
        user_id: 6,
        user_name: "Nguyễn Văn F",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    },
    {
        user_id: 8,
        user_name: "Nguyễn Văn G",
        bangCong: [
            {
                work: "O",
                note: ""
            },
            {

            },
            {

            },
            {
                work: "1",
                note: "Test"
            },
            {
                work: "AL",
                note: "Nghỉ phép năm"
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
            {
                work: "1",
                note: ""
            },
        ]
    }
]
export const chamMay = [
    {
        user_id: 1,
        user_name: "Nguyễn Văn A",
        bangCong: [
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {

            },
            {

            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{
                    vao: "18:00",
                    ra: "21:00"
                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: "test"
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:15",
                    ra: "17:30"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:00",
                    ra: "18:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    
                },
                tangCa:{

                },
                note: ""
            },
        ]
    },
    {
        user_id: 1,
        user_name: "Nguyễn Văn A",
        bangCong: [
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {

            },
            {

            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{
                    vao: "18:00",
                    ra: "21:00"
                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: "test"
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:15",
                    ra: "17:30"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:00",
                    ra: "18:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    
                },
                tangCa:{

                },
                note: ""
            },
        ]
    },
    {
        user_id: 1,
        user_name: "Nguyễn Văn A",
        bangCong: [
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {

            },
            {

            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{
                    vao: "18:00",
                    ra: "21:00"
                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: "test"
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:15",
                    ra: "17:30"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:00",
                    ra: "18:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    
                },
                tangCa:{

                },
                note: ""
            },
        ]
    },
    {
        user_id: 1,
        user_name: "Nguyễn Văn A",
        bangCong: [
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {

            },
            {

            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{
                    vao: "18:00",
                    ra: "21:00"
                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: "test"
            },
            {
                hanhChinh: {
                    vao: "08:30",
                    ra: "17:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:15",
                    ra: "17:30"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    vao: "08:00",
                    ra: "18:00"
                },
                tangCa:{

                },
                note: ""
            },
            {
                hanhChinh: {
                    
                },
                tangCa:{

                },
                note: ""
            },
        ]
    },

]
export const nhanVienInfo = {
    user_id : 1,
    user_fullname : "Ngọc Thương",
    user_chucVu: "Nhân Viên",
    user_ccccd: "0510 2500 9999",
    user_birthday: "2000-01-01",
    user_gender: "Nữ",
    user_hocVan: "12/12",
    user_phone: "0977 777 777",
    user_start: "2023-03-31",
    user_avatar: "https://haycafe.vn/wp-content/uploads/2022/11/Hinh-anh-meme-cheems.jpg",
    hopDong:{
        loaiHopDong: 0,
        expiredDay: "2023-12-01",
        img: "https://haycafe.vn/wp-content/uploads/2022/11/Hinh-anh-meme-cheems.jpg"
    },
    cccd:{
        cccd_matTruoc: "https://static.ttbc-hcm.gov.vn/images/upload/lienphuong/04102021//cccd_bodm.jpg",
        cccd_matSau : "https://static.ttbc-hcm.gov.vn/images/upload/lienphuong/04102021//cccd_bodm.jpg",
        cccd_expired: "2035-01-01"
    },
    gplx:{
        gplx_matTruoc: "https://cdn.thuvienphapluat.vn/uploads/tintuc/2023/07/06/giam-le-phi-cap-giay-phep-lai-xe.png",
        gplx_matSau: "https://cdn.thuvienphapluat.vn/uploads/tintuc/2023/07/06/giam-le-phi-cap-giay-phep-lai-xe.png",
        gplx_expired: "2025-12-12"
    },
    chungChi:[
        {
            id:1,
            chungChi_name: "Bằng Tốt Nghiệp THPT",
            chungChi_date: "2023-09-15",
            chungChi_file: "https://cdn.luatminhkhue.vn/lmk/articles/95/477524/bang-tot-nghiep-thpt-co-xep-loai-khong-477524.jpg"
        },
        {
            id:1,
            chungChi_name: "Chứng Chỉ Tiếng Anh",
            chungChi_date: "2023-09-15",
            chungChi_file: "https://cdn.luatminhkhue.vn/lmk/articles/95/477524/bang-tot-nghiep-thpt-co-xep-loai-khong-477524.jpg"
        }
    ]
}