import { createSlice } from "@reduxjs/toolkit";
import { sessionService } from "../../services/sessionServices";
import { QUANLY_TAILIEU_USER } from "../../services/configURL";

const initialState = {
  userInfor: sessionService.getItem(QUANLY_TAILIEU_USER),
  phongBan: 0,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = { ...action.payload };
    },
    setPhongBan: (state, action) => {
      state.phongBan = { ...action.payload };
    },
  },
});

export const { setUserInfor, setPhongBan } = userSlice.actions;

export default userSlice.reducer;
