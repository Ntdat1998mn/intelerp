import { createSlice } from "@reduxjs/toolkit";
// import { sessionService } from "../../services/sessionService";

const initialState = {
  // userInfor: sessionService.getUser(),
  chamCong: 0
};

export const chamCongSlice = createSlice({
  name: "chamCong",
  initialState,
  reducers: {
    setChamCong: (state, action) => {
        state.chamCong = action.payload;
        }
    }
});

export const { setChamCong } = chamCongSlice.actions;

export default chamCongSlice.reducer;
