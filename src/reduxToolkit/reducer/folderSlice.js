import { createSlice } from "@reduxjs/toolkit";
import {
  findAndChangeNameByFolderId,
  findAndSpliceFolderByFolderId,
  findAndPushFolderByFolderId,
  findPathByFolderId,
} from "../../utils/folderUtils";

const initialState = {
  initialFolderList: [
    {
      id: 1,
      name: "Phòng kế toán",
      isDirectory: true,
      path: ".",
      children: [
        {
          id: 2,
          name: "Kế toán thuế",
          isDirectory: true,
          path: "./1",
          children: [
            {
              id: 3,
              name: "Folder 10",
              path: "./1/2",
              isDirectory: true,
              children: [],
            },
            {
              id: 4,
              name: "Folder 20",
              path: "./1/2",
              isDirectory: true,
              children: [],
            },
            {
              id: 17,
              name: "File 8",
              path: "./7/10",
              isDirectory: false,
            },
            {
              id: 18,
              name: "File 9",
              path: "./7/10",
              isDirectory: false,
            },
          ],
        },
        {
          id: 5,
          name: "Kế toán thu chi",
          isDirectory: true,
          path: "./1",
          children: [
            {
              id: 6,
              name: "Folder",
              path: "./1/5",
              isDirectory: true,
              children: [],
            },
          ],
        },
        {
          id: 19,
          name: "File 8",
          path: "./7/10",
          isDirectory: false,
        },
        {
          id: 20,
          name: "File 9",
          path: "./7/10",
          isDirectory: false,
        },
      ],
    },
    {
      id: 7,
      name: "Phòng tư vấn thiết kế",
      isDirectory: true,
      path: ".",
      children: [
        {
          id: 8,
          name: "Folder 1",
          isDirectory: true,
          children: [
            {
              id: 9,
              name: "File 1",
              path: "./7",
              isDirectory: true,
              children: [],
            },
          ],
        },
        {
          id: 10,
          name: "Folder 2",
          path: "./7",
          isDirectory: true,
          children: [
            {
              id: 11,
              name: "File 2",
              path: "./7/10",
              isDirectory: true,
              children: [],
            },
            {
              id: 12,
              name: "Subfolder",
              isDirectory: true,
              children: [
                {
                  id: 13,
                  name: "File 3",
                  path: "./7/10",
                  isDirectory: true,
                  children: [],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: 14,
      name: "Phòng nhân sự",
      path: ".",
      isDirectory: true,
      children: [],
    },
  ],
  folderList: [
    {
      id: 1,
      name: "Phòng kế toán",
      isDirectory: true,
      path: ".",
      children: [
        {
          id: 2,
          name: "Kế toán thuế",
          isDirectory: true,
          path: "./1",
          children: [
            {
              id: 3,
              name: "Folder 10",
              path: "./1/2",
              isDirectory: true,
              children: [],
            },
            {
              id: 4,
              name: "Folder 20",
              path: "./1/2",
              isDirectory: true,
              children: [],
            },
            {
              id: 17,
              name: "File 8",
              path: "./7/10",
              isDirectory: false,
            },
            {
              id: 18,
              name: "File 9",
              path: "./7/10",
              isDirectory: false,
            },
          ],
        },
        {
          id: 5,
          name: "Kế toán thu chi",
          isDirectory: true,
          path: "./1",
          children: [
            {
              id: 6,
              name: "Folder",
              path: "./1/5",
              isDirectory: true,
              children: [],
            },
          ],
        },
        {
          id: 19,
          name: "File 8",
          path: "./7/10",
          isDirectory: false,
        },
        {
          id: 20,
          name: "File 9",
          path: "./7/10",
          isDirectory: false,
        },
      ],
    },
    {
      id: 7,
      name: "Phòng tư vấn thiết kế",
      isDirectory: true,
      path: ".",
      children: [
        {
          id: 8,
          name: "Folder 1",
          isDirectory: true,
          children: [
            {
              id: 9,
              name: "File 1",
              path: "./7",
              isDirectory: true,
              children: [],
            },
          ],
        },
        {
          id: 10,
          name: "Folder 2",
          path: "./7",
          isDirectory: true,
          children: [
            {
              id: 11,
              name: "File 2",
              path: "./7/10",
              isDirectory: true,
              children: [],
            },
            {
              id: 12,
              name: "Subfolder",
              isDirectory: true,
              children: [
                {
                  id: 13,
                  name: "File 3",
                  path: "./7/10",
                  isDirectory: true,
                  children: [],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: 14,
      name: "Phòng nhân sự",
      path: ".",
      isDirectory: true,
      children: [],
    },
  ],
  folderForm: { name: "" },
  pathList: [[]],
  currentPathIndex: 0,
  activeFolder: null,
  editingFolderId: { folderList: null, folderTree: null },
  /*   copyingFolderId: null, */
  showShareModal: false,
  showMenuHandleFolder: {
    folderTree: null,
    folderList: null,
  },
};

export const folderSlice = createSlice({
  name: "folder",
  initialState,
  reducers: {
    setInitialFolderList: (state, action) => {
      state.initialFolderList = action.payload;
    },
    setFolderList: (state, action) => {
      state.folderList = action.payload;
    },
    // PathList
    setPathList: (state, action) => {
      let clonePathList = [...state.pathList];
      let newPath = findPathByFolderId(state.folderList, action.payload);
      clonePathList = [...clonePathList, newPath];
      if (state.pathList.length > 10) {
        clonePathList.shift();
      }
      state.currentPathIndex = clonePathList.length - 1;
      state.pathList = clonePathList;
    },
    setActiveFolder: (state, action) => {
      state.folderForm.name = action.payload.name;
      state.activeFolder = action.payload;
    },
    setFolderForm: (state, action) => {
      state.folderForm = { ...state.folderForm, ...action.payload };
    },
    accessBackPath: (state) => {
      state.currentPathIndex -= 1;
      let currentArray = state.pathList[state.currentPathIndex];
      state.activeFolder = currentArray[currentArray.length - 1];
    },
    accessNextPath: (state) => {
      state.currentPathIndex += 1;
      let currentArray = state.pathList[state.currentPathIndex];
      state.activeFolder = currentArray[currentArray.length - 1];
    },
    // Edit thư mục
    setEditingFolderId: (state, action) => {
      state.editingFolderId = { ...state.editingFolderId, ...action.payload };
    },
    submitEditFolderInFolderList: (state, action) => {
      let cloneFolderList = [...state.folderList];
      state.folderList = findAndChangeNameByFolderId(
        cloneFolderList,
        action.payload,
        state.editingFolderId.folderList
      );
      // Chỉnh lại đường dẫn khi đổi tên
      let clonePathList = [...state.pathList];
      let newPathList = clonePathList.map((path) => {
        if (path.length !== 0) {
          let finalIndex = path.length - 1;
          let newPath = findPathByFolderId(
            state.folderList,
            path[finalIndex].id
          );
          return newPath;
        }
      });

      state.pathList = newPathList;
    },
    submitEditFolderInFolderTree: (state, action) => {
      let cloneFolderList = [...state.folderList];
      state.folderList = findAndChangeNameByFolderId(
        cloneFolderList,
        action.payload,
        state.editingFolderId.folderTree
      );
      // Chỉnh lại đường dẫn khi đổi tên
      let clonePathList = [...state.pathList];
      let newPathList = clonePathList.map((path) => {
        if (path.length !== 0) {
          let finalIndex = path.length - 1;
          let newPath = findPathByFolderId(
            state.folderList,
            path[finalIndex].id
          );
          return newPath;
        } else return [];
      });

      state.pathList = newPathList;
    },
    dropFolder: (state, action) => {
      let cloneFolderList = [...state.folderList];
      let inputFolder = findAndSpliceFolderByFolderId(
        cloneFolderList,
        action.payload.droppedFolderId
      );
      let newFolderList = findAndPushFolderByFolderId(
        cloneFolderList,
        action.payload.targetFolderId,
        inputFolder
      );
      state.folderList = newFolderList;
      // Chỉnh lại đường
      let clonePathList = [...state.pathList];
      let newPathList = clonePathList.map((path) => {
        if (path.length !== 0) {
          let finalIndex = path.length - 1;
          let newPath = findPathByFolderId(
            state.folderList,
            path[finalIndex].id
          );
          return newPath;
        } else return [];
      });

      state.pathList = newPathList;
    },
    // Xoá thư mục
    deleteFolder: (state, action) => {
      let cloneFolderList = [...state.folderList];
      findAndSpliceFolderByFolderId(cloneFolderList, action.payload);
      state.folderList = cloneFolderList;
    },
    setShowMenuHandleFolder: (state, action) => {
      state.showMenuHandleFolder = action.payload;
    },
    setShowShareModal: (state, action) => {
      state.showShareModal = action.payload;
    },
    /*     setCopyingFolderId: (state, action) => {
      state.copyingFolderId = action.payload;
    }, */
  },
});

export const {
  setFolderList,
  setPathList,
  setActiveFolder,
  setEditingFolderId,
  submitEditFolderInFolderList,
  submitEditFolderInFolderTree,
  deleteFolder,
  setShowShareModal,
  setShowMenuHandleFolder,
  setFolderForm,
  setCopyingFolderId,
  dropFolder,
  accessBackPath,
  accessNextPath,
} = folderSlice.actions;

export default folderSlice.reducer;
