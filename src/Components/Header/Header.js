import React from "react";
import logo from "../../issets/img/logo.jpg";
import { fileLineSVG, layerSVG, menuSVG, userSVG } from "../../issets/img/svg";
import { Tooltip } from "antd";
import { NavLink } from "react-router-dom";

export default function Header() {
  const menuExpand = (
    <div>
      <NavLink
        to="/"
        activeclassname="active"
        className="flex hover:bg-white hover:text-sky-400 rounded p-2 fill-sky-400"
      >
        <span className="mr-2">{fileLineSVG}</span> <span>Tài liệu nội bộ</span>
      </NavLink>
      <NavLink
        to="/nhan-su"
        activeclassname="active"
        className="flex hover:bg-white hover:text-sky-400 rounded p-2 fill-sky-400"
      >
        <span className="mr-2">{layerSVG}</span> <span>Quản lý nhân sự</span>
      </NavLink>
    </div>
  );
  return (
    <header className="h-12 bg-sky-400 text-white flex justify-between items-center w-100 z-50 relative">
      <div className="flex h-full ">
        <img className="object-cover w-28" src={logo} alt="Logo" />
      </div>
      <div className="flex">
        <div className="ml-3">
          <div className="flex items-center">
            <div className="bg-gray-300 rounded-l-full pl-4 pr-6 py-1 -mr-2">
              Khách
            </div>
            <div className="fill-sky-400 w-10 h-10 rounded-full flex justify-center items-center bg-white">
              {userSVG}
            </div>
          </div>
        </div>
        <Tooltip
          color="gray"
          placement="bottom"
          title={menuExpand}
          arrow={false}
        >
          <div className="mx-4 my-auto fill-white">{menuSVG}</div>
        </Tooltip>
      </div>
    </header>
  );
}
