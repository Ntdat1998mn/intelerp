import React from "react";
import {
  AiOutlineFile,
  AiOutlineFilePdf,
  AiOutlineFileWord,
} from "react-icons/ai";

export default function FileIcon({ fileType }) {
  const iconSize = 24; // Default icon size is 24px
  if (fileType === "pdf") {
    return <AiOutlineFilePdf size={iconSize} />;
  } else if (fileType === "doc" || fileType === "docx") {
    return <AiOutlineFileWord size={iconSize} />;
  } else {
    return <AiOutlineFile size={iconSize} />;
  }
}
