import React, { useState } from "react";
import FileIcon from "../../../../Components/FileIcon/FileIcon";
import { plusSVG, chatSVG, userSVG } from "../../../../issets/img/svg";
import { useDispatch, useSelector } from "react-redux";
import ShareModal from "../ShareModal/ShareModal";
import { setShowShareModal } from "../../../../reduxToolkit/reducer/folderSlice";

export default function DanhSachTep() {
  let activeFolder = useSelector((state) => state.folderSlice.activeFolder);
  let dispatch = useDispatch();
  let showShareFolderModal = useSelector(
    (state) => state.folderSlice.showShareModal
  );
  /* const addFileExpand = (
    <div className="fill-sky-400">
      <div className="grid grid-cols-2 gap-2 h-52 overflow-y-scroll">
        <div className="flex items-center pr-4 bg-sky-400 rounded-full">
          <div className="border border-sky-400 p-2 rounded-full bg-white">
            {userSVG}
          </div>
          <div className="whitespace-nowrap mr-2">@Đây là tên ksahfk</div>
          <button className="fill-black flex w-3 rotate-45">{plusSVG}</button>
        </div>
        <div className="flex items-center pr-4 bg-sky-400 rounded-full">
          <div className="border border-sky-400 p-2 rounded-full bg-white">
            {userSVG}
          </div>
          <div className="whitespace-nowrap mr-2">@Đây là tên ksahfk</div>
          <button className="fill-black flex w-3 rotate-45">{plusSVG}</button>
        </div>
        <div className="flex items-center pr-4 bg-sky-400 rounded-full">
          <div className="border border-sky-400 p-2 rounded-full bg-white">
            {userSVG}
          </div>
          <div className="whitespace-nowrap mr-2">@Đây là tên ksahfk</div>
          <button className="fill-black flex w-3 rotate-45">{plusSVG}</button>
        </div>
      </div>
      <div className="flex items-center my-4 border border-sky-400 rounded overflow-hidden p-2">
        <input
          className="flex-1 text-gray-400 focus:outline-none"
          type="text"
          placeholder="Tìm kiếm"
        />
        <button>{searchSVG}</button>
      </div>
    </div>
  ); */
  /* Render ngườiaddFileExpand xem */
  const [viewerList, setViewerList] = useState([1, 1, 1, 1]);
  const renderViewer = (viewerList) => {
    return viewerList.slice(0, 3).map((viewer, index) => {
      return (
        <div
          key={index}
          className="flex-col flex justify-center items-center mr-2"
        >
          <div className="border border-sky-400 rounded-full w-8 h-8 flex justify-center items-center">
            {userSVG}
          </div>
          <div className="text-purple-400 break-all">@Đây là tên</div>
        </div>
      );
    });
  };
  /* Render file list */

  const renderFileList = (folderList) => {
    return folderList.map((folder, index) => {
      if (!folder.isDirectory) {
        return (
          <tr key={index}>
            <td className="border border-gray-300 p-2">
              {
                <div className="flex justify-between items-center">
                  <div className="flex items-end">
                    <div className="mr-2">
                      <FileIcon fileType={"doc"} />
                    </div>
                    <p className="text-black">{folder.name}</p>
                  </div>
                  <button className="btn">{chatSVG}</button>
                </div>
              }
            </td>
            <td className="border border-gray-300 p-2">
              <div className="flex justify-between">
                <div className="flex pr-2">
                  {renderViewer(viewerList)}
                  {viewerList.length > 3 && (
                    <>
                      <div className="flex item-start z-30">
                        <div className="border bg-white border-sky-400 rounded-full w-8 h-8 flex justify-center items-center">
                          {userSVG}
                        </div>
                      </div>
                      <div className="flex item-start z-20 -mx-6">
                        <div className="border bg-white border-sky-400 rounded-full w-8 h-8 flex justify-center items-center">
                          {userSVG}
                        </div>
                      </div>
                      <div className="flex item-start z-10">
                        <div className="border bg-white border-sky-400 rounded-full w-8 h-8 flex justify-center items-center">
                          {userSVG}
                        </div>
                      </div>
                      <div></div>
                    </>
                  )}
                </div>

                <button
                  onClick={() => {
                    dispatch(setShowShareModal(true));
                  }}
                  className="bg-sky-400 px-2 h-8 my-auto whitespace-nowrap rounded text-white active:bg-gray-400"
                >
                  Chia sẻ
                </button>
              </div>
            </td>
            <td className="border border-gray-300 p-2">
              <div className="flex justify-center items-center mr-2">
                <div className="border border-sky-400 rounded-full w-8 h-8 flex justify-center items-center">
                  {userSVG}
                </div>
                <div className="flex flex-col justify-center break-all ml-2">
                  <p className="text-purple-400">@Đây là tên</p>
                  <p> 12/11/2023</p>
                </div>
              </div>
            </td>
            <td className="border border-gray-300 p-2">File về kế toán</td>
          </tr>
        );
      }
    });
  };
  return (
    <div className="fill-sky-400">
      <div className="w-full rounded-t border-l-2 border-sky-400">
        <table className="w-full text-center">
          <thead>
            <tr className="text-sky-400 w-full">
              <th className="border border-gray-300 p-2 w-3/12">TÀI LIỆU</th>
              <th className="border border-gray-300 p-2 w-4/12">NGƯỜI XEM</th>
              <th className="border border-gray-300 p-2 w-2/12">
                UPLOAD LẦN CUỐI
              </th>
              <th className="border border-gray-300 p-2 w-3/12">GHI CHÚ</th>
            </tr>
          </thead>
          <tbody>
            {renderFileList(activeFolder ? activeFolder.children : [])}
          </tbody>
        </table>
      </div>
      <div className="w-full rounded-b border-l-2 border-sky-200">
        <button className="flex items-center w-full border border-t-0 border-gray-300 p-2 text-left">
          <span className="fill-sky-400 mr-2">{plusSVG}</span>
          <span> Thêm tài liệu</span>
        </button>
      </div>
      {showShareFolderModal && <ShareModal />}
    </div>
  );
}
