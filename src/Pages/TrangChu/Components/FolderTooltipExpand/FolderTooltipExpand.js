import React from "react";
import {
  copySVG,
  deleteSVG,
  editSVG,
  pasteSVG,
} from "../../../../issets/img/svg";
import { useDispatch } from "react-redux";
import {
  deleteFolder,
  setShowMenuHandleFolder,
} from "../../../../reduxToolkit/reducer/folderSlice";
import { handleEditName } from "../../../../utils/folderUtils";

export default function FolderTooltipExpand({ folder, folderType }) {
  let dispatch = useDispatch();
  return (
    <div
      onClick={() => {
        dispatch(
          setShowMenuHandleFolder({
            folderList: null,
            folderTree: null,
          })
        );
      }}
      className="text-sky-400 fill-sky-400"
    >
      <button className="flex items-center">
        <span className="mx-1 w-4">{copySVG}</span>
        <span>Sao chép</span>
      </button>
      <button className="flex items-center">
        <span className="mx-1 w-4">{pasteSVG}</span>
        <span>Dán</span>
      </button>
      <button
        onClick={() => {
          handleEditName(dispatch, folder, folderType);
        }}
        className="flex items-center"
      >
        <span className="mx-1">{editSVG}</span>
        <span>Sửa tên</span>
      </button>
      <button
        onClick={() => {
          dispatch(deleteFolder(folder.id));
        }}
        className="flex items-center"
      >
        <span className="mx-1">{deleteSVG}</span>
        <span>Xoá thư mục</span>
      </button>
    </div>
  );
}
