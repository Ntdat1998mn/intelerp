import React, { useEffect, useState } from "react";
import folderImg from "../../../../issets/img/folder.jpg";
import {
  arrowLeftSVG,
  arrowRightSVG,
  refreshSVG,
  searchSVG,
} from "../../../../issets/img/svg";
import { AutoComplete } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  accessBackPath,
  accessNextPath,
  setPathList,
} from "../../../../reduxToolkit/reducer/folderSlice";

export default function DuongDanVaThanhTimKiem() {
  let dispatch = useDispatch();
  let currentPathIndex = useSelector(
    (state) => state.folderSlice.currentPathIndex
  );

  let activeFolder = useSelector((state) => state.folderSlice.activeFolder);
  let folderList = useSelector((state) => state.folderSlice.folderList);
  let pathList = useSelector((state) => state.folderSlice.pathList);

  let currentPath = pathList[currentPathIndex];
  const [searchValue, setSearchValue] = useState("");
  const handleChangeSearchForm = (e) => {
    let { value } = e.target;
    setSearchValue(value);
  };
  /* Autocomplete antd */
  const highlightMatches = (text, query) => {
    const regex = new RegExp(`(${query})`, "gi");
    return text.replace(regex, `<span class="text-sky-400">${query}</span>`);
  };
  const searchResult = (query, folderList, result = []) => {
    const searchFolder = (folderList) => {
      for (let folder of folderList) {
        if (folder.name.toLowerCase().includes(query.toLowerCase())) {
          result.push({ value: folder.name });
        }
        if (folder.children && folder.children.length > 0) {
          searchFolder(folder.children);
        }
      }
    };
    searchFolder(folderList);
    return result.map((folder, index) => {
      return {
        value: folder.value,
        label: (
          <div
            key={index}
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <span
              dangerouslySetInnerHTML={{
                __html: highlightMatches(folder.value, query),
              }}
            ></span>
          </div>
        ),
      };
    });
  };
  const [options, setOptions] = useState([]);
  const handleSearch = (value) => {
    setOptions(value ? searchResult(value, folderList) : []);
  };
  const onSelect = (value, option) => {
    setSearchValue(value);
  };
  /* Render đường dẫn thư mục */
  const renderPathBar = (currentPath) => {
    let newCurrentPath = currentPath;
    let currentPathLength = currentPath.length;
    if (currentPathLength > 5) {
      newCurrentPath = currentPath.slice(
        currentPathLength - 5,
        currentPathLength
      );
      newCurrentPath.unshift({ name: "..." });
    }
    return newCurrentPath.map((folder, index) => {
      return (
        <div key={index} className="flex">
          <div className="flex items-center m-1">
            <svg height="0.8em" viewBox="0 0 320 512">
              <path d="M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z" />
            </svg>
          </div>
          <button
            onClick={() => {
              dispatch(setPathList(folder.id));
            }}
            className="whitespace-nowrap"
          >
            {folder.name}
          </button>
        </div>
      );
    });
  };
  useEffect(() => {
    dispatch(setPathList(activeFolder?.id));
  }, [folderList]);
  return (
    <div className="search_bar flex justify-between text-black py-1 px-4">
      <div className="flex items-center fill-gray-400">
        <button
          onClick={() => {
            dispatch(accessBackPath());
          }}
          disabled={currentPathIndex === 0}
          className={`mr-4 ${currentPathIndex !== 0 && "fill-sky-400"}`}
        >
          {arrowLeftSVG}
        </button>
        <button
          onClick={() => {
            dispatch(accessNextPath());
          }}
          disabled={currentPathIndex === pathList.length - 1}
          className={`${
            currentPathIndex !== pathList.length - 1 && "fill-sky-400"
          }`}
        >
          {arrowRightSVG}
        </button>
      </div>
      <div className="w-8/12 rounded border border-sky-400 px-2 flex justify-between items-center min-[1200px]:-ml-10">
        <div className="flex fill-sky-400">
          <div className="flex items-center">
            <img className="w-6" src={folderImg} alt="" />
          </div>
          <div className="flex pr-0">{renderPathBar(currentPath)}</div>
        </div>
        <button className="flex items-center fill-sky-400">{refreshSVG}</button>
      </div>
      <div
        className="flex w-3/12 items-center bg-white rounded border
      border-sky-400"
      >
        <AutoComplete
          style={{ width: "100%" }}
          options={options}
          onSelect={onSelect}
          onSearch={handleSearch}
        >
          <div className="w-full flex p-2">
            <input
              className="flex-1 focus:outline-none"
              type="text"
              placeholder="Tìm kiếm"
              value={searchValue}
              onChange={handleChangeSearchForm}
            />
            <button className="fill-sky-400 w-4">{searchSVG}</button>
          </div>
        </AutoComplete>
      </div>
    </div>
  );
}
