import React, { useState } from "react";
import { AutoComplete } from "antd";
import {
  arrowRightSVG,
  deleteSVG,
  plusSVG,
  searchSVG,
  userSVG,
} from "../../../../issets/img/svg";
import { useDispatch } from "react-redux";
import { setShowShareModal } from "../../../../reduxToolkit/reducer/folderSlice";
import { getUniqueItems } from "../../../../utils/staffUtils";

export default function ShareModal() {
  let dispatch = useDispatch();
  const handleSetShowShareFolder = () => {
    dispatch(setShowShareModal(false));
  };
  const danhSachPhongBan = [
    { id: 1, name: "Ban giám đốc" },
    { id: 2, name: "Ban quản lý" },
    { id: 3, name: "Ban sản xuất" },
    { id: 4, name: "Phòng kinh doanh" },
    { id: 5, name: "Phòng marketing" },
    { id: 6, name: "Phòng nhân sự" },
  ];
  const renderOptionPhongBan = (options) => {
    return options?.map((option, index) => {
      return (
        <option
          value={option.id}
          key={index}
          className="bg-gray-100 text-black"
        >
          {option.name}
        </option>
      );
    });
  };
  const [searchValue, setSearchValue] = useState("");
  const handleChangeSearchForm = (e) => {
    let { value } = e.target;
    setSearchValue(value);
  };
  /* Autocomplete antd */
  const danhSachNhanVien = [
    { id: 1, name: "Người 1", departmentId: 1 },
    { id: 2, name: "Người 2", departmentId: 2 },
    { id: 3, name: "Người 3", departmentId: 3 },
    { id: 4, name: "Người 4", departmentId: 4 },
    { id: 5, name: "Người 5", departmentId: 5 },
    { id: 6, name: "Người 6", departmentId: 6 },
    { id: 7, name: "Người 7", departmentId: 1 },
    { id: 8, name: "Người 8", departmentId: 2 },
    { id: 9, name: "Người 9", departmentId: 3 },
    { id: 10, name: "Người 10", departmentId: 4 },
    { id: 11, name: "Người 11", departmentId: 4 },
    { id: 12, name: "Người 12", departmentId: 4 },
    { id: 13, name: "Người 13", departmentId: 4 },
    { id: 14, name: "Người 14", departmentId: 4 },
    { id: 10, name: "Người 10", departmentId: 4 },
    { id: 11, name: "Người 11", departmentId: 4 },
    { id: 12, name: "Người 12", departmentId: 4 },
    { id: 13, name: "Người 13", departmentId: 4 },
    { id: 14, name: "Người 14", departmentId: 4 },
  ];
  const [selectedStaffList, setSelectedStaffList] = useState([
    { id: 15, name: "Người 15", departmentId: 4 },
    { id: 16, name: "Người 16", departmentId: 4 },
    { id: 17, name: "Người 17", departmentId: 4 },
    { id: 18, name: "Người 18", departmentId: 4 },
    { id: 19, name: "Người 19", departmentId: 4 },
  ]);

  const [unselectedStaffList, setUnselectedStaffList] =
    useState(danhSachNhanVien);
  let [selectingStaffList, setSelectingStaffList] = useState([]);

  const searchResult = (query) =>
    unselectedStaffList
      .filter((nhanVien) =>
        nhanVien.name.toLowerCase().includes(query.toLowerCase())
      )
      .map((nhanVien) => {
        const highlightedName = nhanVien.name.replace(
          new RegExp(query, "gi"),
          `<span class="text-sky-400">${query}</span>`
        );
        return {
          value: nhanVien.name,
          label: (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <span
                dangerouslySetInnerHTML={{ __html: highlightedName }}
              ></span>
            </div>
          ),
        };
      });

  const [options, setOptions] = useState([]);
  const handleSearch = (value) => {
    setOptions(value ? searchResult(value) : []);
  };
  const onSelect = (value, option) => {
    setSearchValue(value);
  };
  // Thay đổi danh sách nhân viên khi chọn phòng
  const handleChangeDepartment = (e) => {
    let departmentId = e.target.value;
    if (departmentId == 0) {
      let newUnselectStaff = getUniqueItems(
        danhSachNhanVien,
        selectingStaffList,
        selectedStaffList
      );
      setUnselectedStaffList(newUnselectStaff);
    } else {
      let newUnselectedStaffList = danhSachNhanVien.filter((staff) => {
        return staff.departmentId == departmentId;
      });
      let newUnselectStaff = getUniqueItems(
        newUnselectedStaffList,
        selectingStaffList,
        selectedStaffList
      );
      setUnselectedStaffList(newUnselectStaff);
    }
  };
  // Chọn từng thành viên trong nhóm
  const handleSelectStaff = (selectingStaff, index) => {
    unselectedStaffList.splice(index, 1);
    setSelectingStaffList([...selectingStaffList, selectingStaff]);
  };
  // Chọn tất cả thành viên
  const handleSelectAllStaff = () => {
    setSelectingStaffList([...selectingStaffList, ...unselectedStaffList]);
    setUnselectedStaffList([]);
  };
  // render danh sách người chưa có quyền xem
  const renderUnselectedStaffList = (staffList) => {
    return staffList.map((staff, index) => {
      return (
        <div
          onClick={() => {
            handleSelectStaff(staff, index);
          }}
          key={index}
          className="flex items-center cursor-pointer bg-gray-200 rounded-full"
        >
          <span className="flex items-center justify-center fill-sky-400 border border-sky-400 bg-white w-8 h-8 rounded-full">
            {userSVG}
          </span>
          <span className="ml-2 text-black">{staff.name}</span>
        </div>
      );
    });
  };
  // render danh sách người đang chọn để thêm quyền xem
  const renderSelectingStaffList = (staffList) => {
    return staffList.map((staff, index) => {
      return (
        <div key={index} className="mr-2 relative">
          <button
            onClick={() => {
              selectingStaffList.splice(index, 1);
              setUnselectedStaffList([...unselectedStaffList, staff]);
            }}
            className="bg-gray-400 fill-white w-3 h-3 flex justify-center items-center rotate-45 rounded-full p-0.5 absolute top-0 right-0"
          >
            {plusSVG}
          </button>
          <div className="flex items-center justify-center fill-sky-400 border border-sky-400 bg-white w-8 h-8 rounded-full mx-auto">
            {userSVG}
          </div>
          <div className="whitespace-nowrap">{staff.name}</div>
        </div>
      );
    });
  };
  // render danh sách người đã có quyền xem
  const renderSelectedStaffList = (staffList) => {
    return staffList.map((staff, index) => {
      return (
        <div
          key={index}
          className="flex items-center bg-gray-200 rounded-full w-4/5 mx-auto my-2"
        >
          <span className="flex items-center justify-center fill-sky-400 border border-sky-400 bg-white w-8 h-8 rounded-full">
            {userSVG}
          </span>
          <span className="flex-1 ml-2 text-black">{staff.name}</span>
          <button
            onClick={() => {
              selectedStaffList.splice(index, 1);
              setSelectedStaffList([...selectedStaffList]);
            }}
            className="fill-sky-400 active:fill-green-400 mr-2"
          >
            {deleteSVG}
          </button>
        </div>
      );
    });
  };

  return (
    <div className="w-full h-full modal">
      <div className="fixed h-screen w-screen opacity-30 top-0 left-00 bg-sky-400 z-30"></div>
      <div className="modal-container">
        <button
          onClick={handleSetShowShareFolder}
          className="absolute flex items-center justify-center active:bg-green-400 rotate-45 fill-white -top-6 -right-6 rounded-full bg-gray-400 w-8 h-8"
        >
          {plusSVG}
        </button>
        <div className="flex bg-white overflow-hidden rounded-xl">
          <div className="w-2/3 border-r-2 border-gray-400">
            <div className="bg-sky-400 text-white text-center rounded-es-xl text-base font-medium py-4">
              Chia sẻ "Tên phòng"
            </div>
            <div className="px-10 py-2 border-b-2 border-gray-400">
              <div className="flex justify-between items-center">
                <div className="flex items-center bg-sky-400 text-white rounded">
                  <button onClick={handleSelectAllStaff} className="px-2 py-1">
                    Chọn thành viên
                  </button>
                </div>
                <div>
                  <select
                    onChange={handleChangeDepartment}
                    className="bg-sky-400 rounded text-white px-2 py-1 focus:outline-none outline-none"
                  >
                    <option
                      value={0}
                      className="bg-gray-100 text-black outline-none border-none"
                    >
                      Tất cả
                    </option>
                    {renderOptionPhongBan(danhSachPhongBan)}
                  </select>
                </div>
              </div>
              <div className="border border-sky-400 rounded-full mt-2">
                <AutoComplete
                  style={{ width: "100%" }}
                  options={options}
                  onSelect={onSelect}
                  onSearch={handleSearch}
                >
                  <div className="w-full flex px-4 py-1">
                    <input
                      className="flex-1 focus:outline-none"
                      type="text"
                      placeholder="Tìm kiếm"
                      value={searchValue}
                      onChange={handleChangeSearchForm}
                    />
                    <button className="fill-sky-400 w-4">{searchSVG}</button>
                  </div>
                </AutoComplete>
              </div>

              <div className="w-full h-60 overflow-auto">
                <div className="grid grid-cols-3 gap-4 mt-2 w-full">
                  {renderUnselectedStaffList(unselectedStaffList)}
                </div>
              </div>
            </div>
            <div className="flex items-center">
              <div className="w-5/6 h-28 flex items-center px-4 overflow-x-auto">
                {renderSelectingStaffList(selectingStaffList)}
              </div>
              <div className="flex justify-center items-center w-1/6">
                <button
                  onClick={() => {
                    setSelectingStaffList([]);
                    setSelectedStaffList([
                      ...selectedStaffList,
                      ...selectingStaffList,
                    ]);
                  }}
                  className="active:bg-gray-400 flex justify-center items-center fill-white w-10 h-10 rounded-full bg-sky-400"
                >
                  {arrowRightSVG}
                </button>
              </div>
            </div>
          </div>
          <div className="w-1/3">
            <div className="bg-sky-400 text-white text-center rounded-ee-xl text-base font-medium py-4">
              Danh sách người xem
            </div>
            <div
              style={{
                height: 444,
              }}
              className="p-2 overflow-y-auto"
            >
              {renderSelectedStaffList(selectedStaffList)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
