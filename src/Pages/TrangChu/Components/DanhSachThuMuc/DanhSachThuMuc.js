import React, { useEffect, useRef, useState } from "react";
import { Tooltip } from "antd";
import folderImg from "../../../../issets/img/folder.jpg";
import { useDispatch, useSelector } from "react-redux";
import {
  setActiveFolder,
  setEditingFolderId,
  setPathList,
  setShowMenuHandleFolder,
  submitEditFolderInFolderList,
} from "../../../../reduxToolkit/reducer/folderSlice";
import FolderTooltipExpand from "../FolderTooltipExpand/FolderTooltipExpand";

export default function DanhSachThuMuc() {
  let dispatch = useDispatch();
  let activeFolder = useSelector((state) => state.folderSlice.activeFolder);

  let showMenuHandleFolder = useSelector(
    (state) => state.folderSlice.showMenuHandleFolder
  );
  let editingFolderId = useSelector(
    (state) => state.folderSlice.editingFolderId
  );
  let folderForm = useSelector((state) => state.folderSlice.folderForm);
  const inputFolderNameRef = useRef(folderForm.name);
  // Xác nhận đổi tên
  const handleKeyUp = (event) => {
    if (event.key === "Enter") {
      dispatch(submitEditFolderInFolderList(inputFolderNameRef.current.value));
      dispatch(setEditingFolderId({ folderList: null }));
    }
  };
  /* Render list thư mục */
  const renderFolderList = (folderList) => {
    return folderList.map((folder, index) => {
      if (folder.isDirectory) {
        return (
          <div
            onContextMenu={(e) => {
              e.preventDefault();
              dispatch(
                setShowMenuHandleFolder({
                  folderList: folder.id,
                  folderTree: null,
                })
              );
            }}
            key={index}
            className="text-center z-20"
          >
            <Tooltip
              open={showMenuHandleFolder.folderList === folder.id}
              color="white"
              placement="bottom"
              title={
                <FolderTooltipExpand
                  folder={folder}
                  folderType={"folderList"}
                />
              }
            >
              <div
                onClick={() => {
                  dispatch(setActiveFolder(folder));
                }}
                onDoubleClick={() => {
                  dispatch(setPathList(folder.id));
                  dispatch(setActiveFolder(folder));
                }}
                key={index}
                className={`text-center py-2 ${
                  activeFolder?.id === folder.id
                    ? "bg-blue-300"
                    : "hover:bg-blue-200"
                }`}
              >
                <img className="w-8 mx-auto" src={folderImg} alt="" />
                {editingFolderId.folderList === folder.id ? (
                  <textarea
                    ref={inputFolderNameRef}
                    className="bg-transparent px-1 focus:outline-0 w-full h-max overflow-hidden"
                    type="text"
                    wrap="soft"
                    maxLength={50}
                    required
                    onKeyUp={handleKeyUp}
                  />
                ) : (
                  <p className="break-words">{folder.name}</p>
                )}
              </div>
            </Tooltip>
          </div>
        );
      }
    });
  };
  useEffect(() => {
    if (editingFolderId.folderList) {
      inputFolderNameRef.current.value = folderForm.name;
      inputFolderNameRef.current.focus();
    }
  }, [editingFolderId]);
  return (
    <div className="folder_list grid grid-cols-10 gap-4 py-2">
      {renderFolderList(activeFolder ? activeFolder.children : [])}
    </div>
  );
}
