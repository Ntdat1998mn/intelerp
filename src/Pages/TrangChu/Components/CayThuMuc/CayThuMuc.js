import React, { useEffect, useRef, useState } from "react";
import { Tooltip } from "antd";
import folderImg from "../../../../issets/img/folder.jpg";
import { caretSVG, homeSVG } from "../../../../issets/img/svg";
import {
  setActiveFolder,
  setEditingFolderId,
  setPathList,
  setShowMenuHandleFolder,
  submitEditFolderInFolderTree,
} from "../../../../reduxToolkit/reducer/folderSlice";
import { useDispatch, useSelector } from "react-redux";
import FolderTooltipExpand from "../FolderTooltipExpand/FolderTooltipExpand";
import {
  handleDragOver,
  handleDragStart,
  handleDrop,
} from "../../../../utils/folderUtils";

export default function CayThuMuc() {
  let dispatch = useDispatch();
  let folderList = useSelector((state) => state.folderSlice.folderList);
  let activeFolder = useSelector((state) => state.folderSlice.activeFolder);
  let editingFolderId = useSelector(
    (state) => state.folderSlice.editingFolderId
  );
  let folderForm = useSelector((state) => state.folderSlice.folderForm);
  let showMenuHandleFolder = useSelector(
    (state) => state.folderSlice.showMenuHandleFolder
  );
  const inputFolderNameRef = useRef(folderForm.name);
  /* Đóng mở thư mục theo id */
  const [expandedFolders, setExpandedFolders] = useState([]);
  const toggleFolder = (folderId) => {
    if (expandedFolders.includes(folderId)) {
      setExpandedFolders(expandedFolders.filter((id) => id !== folderId));
    } else {
      setExpandedFolders([...expandedFolders, folderId]);
    }
  };
  // Xử lý khi onClick vào thư mục
  const handleFolderClick = (folder) => {
    toggleFolder(folder.id);
    dispatch(setPathList(folder.id));
    dispatch(setActiveFolder(folder));

    if (
      showMenuHandleFolder.folderTree !== null ||
      showMenuHandleFolder.folderList !== null
    ) {
      dispatch(
        setShowMenuHandleFolder({
          folderList: null,
          folderTree: null,
        })
      );
    }
  };
  // Xác nhận đổi tên
  const handleKeyUp = (event) => {
    if (event.key === "Enter") {
      dispatch(submitEditFolderInFolderTree(inputFolderNameRef.current.value));
      dispatch(setEditingFolderId({ folderTree: null }));
    }
  };
  // Xử lý khi click chuột phải vào thư mục
  const handleContextMenu = (e, folder) => {
    e.preventDefault();
    dispatch(
      setShowMenuHandleFolder({
        folderList: null,
        folderTree: folder.id,
      })
    );
  };

  // Render cây thư mục
  const renderFolderTree = (folderList, callBack) => {
    return folderList.map((folder, index) => {
      if (folder.isDirectory) {
        return (
          <div key={index} className="pl-3">
            <Tooltip
              open={showMenuHandleFolder.folderTree == folder.id}
              color="white"
              placement="bottomLeft"
              title={
                <FolderTooltipExpand
                  folder={folder}
                  folderType={"folderTree"}
                />
              }
              arrow={false}
            >
              <div
                onDragStart={(e) => {
                  handleDragStart(e, folder);
                }}
                onDrop={(e) => {
                  handleDrop(e, folder, dispatch);
                }}
                onDragOver={(e) => {
                  handleDragOver(e);
                }}
                draggable // Đánh dấu phần tử này có thể kéo
                onClick={() => handleFolderClick(folder)}
                onContextMenu={(e) => handleContextMenu(e, folder)}
                className={`relative flex items-center cursor-pointer fill-black z-20 ${
                  activeFolder?.id === folder.id
                    ? "bg-blue-300"
                    : "hover:bg-blue-200"
                }`}
              >
                {expandedFolders.includes(folder.id) &&
                folder.children?.length > 0 ? (
                  <span className="-rotate-90">{caretSVG}</span>
                ) : folder.children?.length > 0 ? (
                  <span className="rotate-180">{caretSVG}</span>
                ) : (
                  <span className="fill-none">{caretSVG}</span>
                )}
                <img className="w-5" src={folderImg} alt="" />
                {editingFolderId.folderTree === folder.id ? (
                  <input
                    onKeyUp={handleKeyUp}
                    ref={inputFolderNameRef}
                    className="bg-transparent px-1 focus:outline-0"
                    type="text"
                    required
                  />
                ) : (
                  <span className="break-all">{folder.name}</span>
                )}
              </div>
            </Tooltip>
            {/* CallBack hàm render để xử lý thư mục chứa thư mục */}
            {expandedFolders.includes(folder.id) &&
              folder.children &&
              callBack(folder.children, renderFolderTree)}
          </div>
        );
      }
    });
  };
  useEffect(() => {
    if (editingFolderId.folderTree) {
      inputFolderNameRef.current.value = folderForm.name;
      inputFolderNameRef.current.focus();
    }
  }, [editingFolderId]);
  return (
    <>
      <button
        onClick={() => {
          dispatch(setPathList(0));
        }}
        className="fill-sky-400 text-sky-400 py-2"
      >
        <span className="flex">
          {homeSVG}
          <span className="ml-1">Trang chủ</span>
        </span>
      </button>
      <div className="w-full h-0 border-b-2 border-gray-400"></div>
      <div className="-ml-3 py-2 folder_tree-list overflow-y-auto ">
        {renderFolderTree(folderList, renderFolderTree)}
      </div>
    </>
  );
}
