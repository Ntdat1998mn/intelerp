import React from "react";
import {
  chatSQSVG,
  copySVG,
  pasteSVG,
  plusSVG,
} from "../../../../issets/img/svg";
import copy from "../../../../issets/img/copy.png";
import paste from "../../../../issets/img/paste.png";
import rename from "../../../../issets/img/rename.png";
import share from "../../../../issets/img/share.png";
import { useDispatch, useSelector } from "react-redux";
import {
  setEditingFolderId,
  setShowShareModal,
} from "../../../../reduxToolkit/reducer/folderSlice";

export default function ThanhCongCu() {
  let dispatch = useDispatch();
  let activeFolder = useSelector((state) => state.folderSlice.activeFolder);
  const handleEditName = () => {
    dispatch(setEditingFolderId({ folderTree: activeFolder?.id }));
  };
  return (
    <div className="flex justify-between items-center h-12 bg-gray-100 px-4">
      <div className="flex">
        <button className="flex">
          <div className="flex justify-center items-center  fill-sky-400 border-2 border-sky-400 rounded-full w-6 h-6 p-1">
            {plusSVG}
          </div>
          <span className="ml-2">Mới</span>
        </button>
        <div className="bg-sky-400 w-0.5 h-6 rounded mx-5"></div>
        <div className="flex items-center h-full fill-sky-400">
          <button className="w-5 h-5">{copySVG}</button>
          <button className="mx-4 w-5 h-5">{pasteSVG}</button>
          <button onClick={handleEditName} className="h-7">
            <img className="h-full" src={rename} />
          </button>
        </div>
        <div className="bg-sky-400 w-0.5 h-6 rounded mx-5"></div>
        <div className="flex items-center fill-white">
          <div>{chatSQSVG}</div> <span className="ml-2"> Chat</span>
        </div>
      </div>
      <button
        onClick={() => {
          dispatch(setShowShareModal(true));
        }}
        className="overflow-hidden w-32"
      >
        <img className="object-cover" src={share} alt="" />
      </button>
    </div>
  );
}
