import React, { useEffect, useRef, useState } from "react";
import ThanhCongCu from "./Components/ThanhCongCu/ThanhCongCu";
import DuongDanVaThanhTimKiem from "./Components/DuongDanVaThanhTimKiem/DuongDanVaThanhTimKiem";
import DanhSachTep from "./Components/DanhSachTep/DanhSachTep";
import ShareModal from "./Components/ShareModal/ShareModal";
import { useDispatch, useSelector } from "react-redux";
import DanhSachThuMuc from "./Components/DanhSachThuMuc/DanhSachThuMuc";
import {
  setFolderList,
  setShowMenuHandleFolder,
} from "../../reduxToolkit/reducer/folderSlice";
import CayThuMuc from "./Components/CayThuMuc/CayThuMuc";
import { folderService } from "../../services/folderServices";
import { showMessage } from "../../utils/commonHandler";

export default function HomePage() {
  const user = useSelector((state) => state.userSlice.userInfor);

  let dispatch = useDispatch();
  let showShareFolderModal = useSelector(
    (state) => state.folderSlice.showShareModal
  );
  let showMenuHandleFolder = useSelector(
    (state) => state.folderSlice.showMenuHandleFolder
  );
  // Khai báo phần kéo thả cây thư mục
  let [folderTreewidth, setFolderTreeWidth] = useState(200);

  const [isResizing, setIsResizing] = useState(false);
  const handleMouseDown = () => {
    setIsResizing(true);
  };
  const handleMouseMove = (e) => {
    e.preventDefault();
    let width = e.clientX;
    if (width > 200 && width < 800) {
      setFolderTreeWidth(width);
    } else {
      if (width < 200) {
        setFolderTreeWidth(200);
      } else if (width > 800) {
        setFolderTreeWidth(800);
      }
    }
  };
  const handleMouseUp = () => {
    setIsResizing(false);
  };
  useEffect(() => {
    if (!user) {
      window.location.href = "/dang-nhap";
    }
    folderService
      .layItemList(user.token)
      .then((res) => {
        dispatch(setFolderList(res.data.content));
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return (
    <section className="content">
      {(showMenuHandleFolder.folderTree !== null) |
      (showMenuHandleFolder.folderList !== null) ? (
        <div
          onClick={() => {
            dispatch(
              setShowMenuHandleFolder({ folderList: null, folderTree: null })
            );
          }}
          onContextMenu={(e) => {
            e.preventDefault();
            dispatch(
              setShowMenuHandleFolder({ folderList: null, folderTree: null })
            );
          }}
          className="overlay_tooltip-folder"
        ></div>
      ) : (
        ""
      )}
      {/* Thanh công cụ */}
      {<ThanhCongCu />}
      {/* Đường dẫn và thanh tìm kiếm */}
      {<DuongDanVaThanhTimKiem />}
      <main
        onMouseMove={(e) => {
          isResizing && handleMouseMove(e);
        }}
        onMouseUp={handleMouseUp}
        className="flex px-4"
      >
        {/* Folder tree */}
        <div style={{ width: `${folderTreewidth}px` }}>
          <div className="flex">
            <div className="w-full">
              <CayThuMuc />
            </div>
            <div onMouseDown={handleMouseDown} className="cursor-ew-resize p-2">
              <div className="bg-gray-400 w-0.5 h-screen"></div>
            </div>
          </div>
        </div>
        <div className="mx-auto flex-1 pl-4">
          {/* Phần nội dung chính */}
          {/* Render thư mục */}
          <DanhSachThuMuc />
          {/* Render danh sách tệp */}
          <DanhSachTep />
        </div>
      </main>
      {showShareFolderModal && <ShareModal />}
    </section>
  );
}
