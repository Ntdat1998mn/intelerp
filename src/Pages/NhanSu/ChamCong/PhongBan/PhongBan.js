import React, { useState } from 'react'
import img_folder from '../../../../issets/img/folder.jpg'
import { nhanSu } from '../../../../services/chamCongService'
import { Popconfirm, Tooltip } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { setPhongBan } from '../../../../reduxToolkit/reducer/userSlice';

export default function PhongBan() {
    let [userList,setUserList] = useState(nhanSu);
    let [cloneUsers,setCloneUsers] = useState(nhanSu);
    let [showTooltipNV,setShowTooltipNV] = useState(0);
    let [showTooltipPB,setShowTooltipPB] = useState(0);
    let [isUpdate,setIsUpdate] = useState(false);
    let searchUser = (e) => {
        let array = [];
        cloneUsers?.map((phongBan) => {
            let isFound = false;
            let nhanVienList = [];
            phongBan.nhanVien?.map((nhanVien) => {
                if(nhanVien?.nhanvien_name.toLowerCase().includes(e.target.value.toLowerCase())){
                    nhanVienList.push(nhanVien);
                    isFound = true;
                }
            })
            if(isFound){
                let data = {
                    phongBan_id: phongBan.phongBan_id,
                    phongBan_name: phongBan.phongBan_name,
                    nhanVien : nhanVienList
                }
                array.push(data);
            }
        })
        setUserList(array);
        userList?.map((phongBan) => {
            if(e.target.value == ""){
                document.getElementById(`btn-open-${phongBan.phongBan_id}`).classList.remove("hidden");
                document.getElementById(`btn-close-${phongBan.phongBan_id}`).classList.add("hidden");
                document.getElementById(`nv-list-${phongBan.phongBan_id}`).classList.add("hidden");
            }else{
                if(phongBan.phongBan_id){
                    document.getElementById(`btn-open-${phongBan.phongBan_id}`).classList.add("hidden");
                    document.getElementById(`btn-close-${phongBan.phongBan_id}`).classList.remove("hidden");
                    document.getElementById(`nv-list-${phongBan.phongBan_id}`).classList.remove("hidden");
                }
            }
        })
    }
    // Xử lý click chuột phải
    let handleShowMenuNV = (e) => {
        setShowTooltipNV(e.target.id);
        e.preventDefault();
    }
    let handleShowMenuPB = (e) => {
        setShowTooltipPB(e.target.id);
        e.preventDefault();
        console.log(e.target.id);
        console.log(showTooltipPB);
    }
    // huỷ bỏ chỉnh sửa khi rời chuột
    let cancelEdit = (PBId,NVId) => {
        document.getElementById(`${PBId}${NVId}`).disabled = true;
    }
    //
    // thêm nhân viên
    let changeInput = (indexPB,indexNV,e) => {
        let array = [...cloneUsers];
        array[indexPB].nhanVien[indexNV].nhanvien_name = e.target.value;
        setUserList(array);
    }
    let handleCreateUpdate = (PBIndex,NVIndex,PBId,NVId,e) => {
        let array = [...userList];
        if(isUpdate){
            // đổi tên
            array[PBIndex].nhanVien[NVIndex].nhanvien_name = e.target.value;
            setCloneUsers(array);
            document.getElementById(`${PBId}${NVId}`).blur();
            document.getElementById(`${PBId}${NVId}`).disabled = true;
        }else{
            // tạo mới
            // let length = array[PBIndex].nhanVien.length;
            // console.log(length);
            // array[PBIndex].nhanVien[length-1].nhanvien_name = e.target.value;
            // setCloneUsers(array);
            document.getElementById(`${PBId}99`).blur();
            document.getElementById(`${PBId}99`).disabled = true;
        }
        toast.success("Cập nhật thành công!!!",{
            position: toast.POSITION.TOP_CENTER
        });
    }
    // thay đổi trên redux
    let handleChangePB = (idPB) => {
        setPhongBan(idPB);
    }
    // 
    let handleAddNV = (idPB) => {
        setShowTooltipNV(0);
        document.getElementById(`NV${idPB}`).classList.remove('hidden');
        document.getElementById(`${idPB}99`).focus();
        setIsUpdate(false);
    }
    let handleRenameNV = (idNV) => {
        setShowTooltipNV(0);
        document.getElementById(idNV).disabled = false;
        document.getElementById(idNV).focus();
        setIsUpdate(true);
    }
    let handleDeleteNV = (idNv) => {
        userList.map((phongBan,indexPB) => {
            phongBan.nhanVien?.map((nhanVien,indexNV) => {
                if(nhanVien.nhanvien_id == idNv){
                    let array = [...userList];
                    array[indexPB].nhanVien.splice(indexNV,1);
                    setUserList(array);
                }
            })
        })
    }
    let handleShowNVList = (id,type) => {
        if(type == 0){
            document.getElementById(`btn-close-${id}`).classList.remove("hidden");
            document.getElementById(`btn-open-${id}`).classList.add("hidden");
            document.getElementById(`nv-list-${id}`).classList.remove("hidden");
        }else{
            document.getElementById(`btn-open-${id}`).classList.remove("hidden");
            document.getElementById(`btn-close-${id}`).classList.add("hidden");
            document.getElementById(`nv-list-${id}`).classList.add("hidden");
        }
        
    }
    let renderNhanSu = () => {
       return userList.map((phongBan,indexPB) => {
            return <div className='pl-4'>
                <div className='flex my-2'>
                    <button onClick={() => handleShowNVList(phongBan.phongBan_id,1)} id={`btn-close-${phongBan.phongBan_id}`} className='hidden'><i className="fa fa-angle-down"></i></button>
                    <button onClick={() => handleShowNVList(phongBan.phongBan_id,0)} id={`btn-open-${phongBan.phongBan_id}`}><i className="fa fa-angle-right"></i></button>
                    <img width={"30px"} src={img_folder} alt="" />
                    <div onMouseLeave={() => setShowTooltipPB(0)} onDoubleClick={()=>handleShowNVList(phongBan.phongBan_id,0)} className='text-lg relative'>
                        <input onContextMenu={(e) => handleShowMenuPB(e)} disabled type="text" value={phongBan.phongBan_name} id={`${phongBan.phongBan_id}`} />
                        <div className={(`${phongBan.phongBan_id}` != showTooltipPB) && "hidden" || "absolute z-10 bg-blue-500 p-3 rounded-lg text-white font-semibold"}>
                                        <div className='my-1'><button >Thêm Phòng Ban Mới</button></div>
                                        <Popconfirm title="Xoá Phòng Ban" description="Xác Nhận Xoá Phòng Ban Này?" okText="Đồng Ý" cancelText="Huỷ Bỏ"
                                        >
                                            <div className='my-1'><button className='text-red-700'>Xoá Phòng Ban</button></div>
                                        </Popconfirm>
                                        <div className='my-1'><button className='text-yellow-500'>Đổi Tên</button></div>
                                    </div>
                    </div>
                </div>
                <div id={`nv-list-${phongBan.phongBan_id}`} className='nv-list pl-6 hidden'>
                    {phongBan.nhanVien?.map((nhanVien,indexNV) =>{
                        return <div  className='flex text-lg'>
                                <i className="fa fa-user text-sky-500 self-center"></i>
                                <div onMouseLeave={() => setShowTooltipNV(0)} className='ml-2 my-1 relative'>
                                    <input onMouseLeave={() => cancelEdit(phongBan.phongBan_id,nhanVien.nhanvien_id)} onKeyDown={(e) => {(e.key == "Enter") && handleCreateUpdate(indexPB,indexNV,phongBan.phongBan_id,nhanVien.nhanvien_id,e)}} onChange={(e) => changeInput(indexPB,indexNV,e)} disabled type="text" value={nhanVien.nhanvien_name} id={`${phongBan.phongBan_id}${nhanVien.nhanvien_id}`} onContextMenu={(e) => handleShowMenuNV(e)} />
                                    <div className={(`${phongBan.phongBan_id}${nhanVien.nhanvien_id}` != showTooltipNV) && "hidden" || "absolute z-10 bg-blue-500 p-3 rounded-lg text-white font-semibold"}>
                                        <div className='my-1'><button onClick={() => handleAddNV(phongBan.phongBan_id)}>Thêm Nhân Viên Mới</button></div>
                                        <Popconfirm title="Xoá Nhân Viên" description="Xác Nhận Xoá Nhân Viên Này?" okText="Đồng Ý" cancelText="Huỷ Bỏ"
                                        onConfirm={()=> handleDeleteNV(nhanVien.nhanvien_id)}
                                        >
                                            <div className='my-1'><button className='text-red-700'>Xoá Nhân Viên</button></div>
                                        </Popconfirm>
                                        <div onClick={() => handleRenameNV(`${phongBan.phongBan_id}${nhanVien.nhanvien_id}`)} className='my-1'><button className='text-yellow-500'>Đổi Tên</button></div>
                                    </div>
                                </div>
                            </div>
                    })}
                    <div id={`NV${phongBan.phongBan_id}`} className='flex text-lg hidden'>
                        <i className="fa fa-user text-sky-500 self-center"></i>
                        <Tooltip placement='top' title="Nhấn Enter Để Lưu Thay Đổi">
                            <input onKeyDown={(e) => {(e.key == "Enter") && handleCreateUpdate(indexPB,0,phongBan.phongBan_id,0,e)}} id={`${phongBan.phongBan_id}99`} type="text" placeholder='Tên Nhân Viên' className='ml-2' />
                        </Tooltip>
                    </div>
                </div>
            </div>
        })
    }
  return (
    <div id='phongBan'>
        <div className='m-3 flex folder pb-3'>
            <img width={"40px"} src={img_folder} alt="" />
            <p className='text-xl text-sky-500'>Phòng/Ban</p>
        </div>
        <div style={{height:"400px"}} className='overflow-y-scroll'>
            <div className='search-input text-center'>
                <input onChange={searchUser} style={{border: "1px solid #d5d5d5",outline:"none"}} type="text" placeholder='Tìm nhân viên...' className='h-full px-3 py-2' />
                <button disabled type="button" className="btn bg-blue-500 h-full px-3 py-2">
                    <i className="fas fa-search text-white"></i>
                </button>
            </div>
            {renderNhanSu()}
            <div id="PB999" className='flex text-lg ml-5 hidden'>
                <img width={"30px"} src={img_folder} alt="" />
                        <Tooltip placement='top' title="Nhấn Enter Để Lưu Thay Đổi">
                            <input id="PB99" type="text" placeholder='Tên Phòng Ban' className='ml-2' />
                        </Tooltip>
                    </div>
        </div>
        <ToastContainer />
    </div>
  )
}
