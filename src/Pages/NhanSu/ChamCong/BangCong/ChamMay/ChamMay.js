import moment from 'moment/moment';
import React from 'react'
import { chamMay } from '../../../../../services/chamCongService';

export default function ChamMay() {
    let sunIndex = [];
    let satIndex = [];
    let renderDay = () => {
        let month = "2023-09";
        // tính số ngày của 1 tháng
        let day = moment(month).daysInMonth();
        let array = [];
        for(let i = 1; i <= day; i++){
            if(i<10){
                i  = "0" + i;
                array.push(i);
            }else{
                array.push(String(i));
            }
        }
        return array.map((item,index) => {
            let date = moment(month + "-" + item).format('ddd');
            if(date == "Sun"){
                sunIndex.push(index);
                
            }else if(date == "Sat"){
                satIndex.push(index);
                
            }
                return <th>
                    <p>{item}</p>
                    {/* <p>{date}</p> */}
                    <div className='grid grid-cols-2'>
                        <div className='bg-yellow-300 text-black'>VÀO</div>
                        <div className='bg-green-400 text-white'>RA</div>
                    </div>
                </th>
        })
    }
    let renderBangCong = () => {
        return chamMay.map((ngayCong) => {
            return <tr>
                <td>{ngayCong.user_name}</td>
                <td>
                    <div style={{borderBottom: "1px solid #119ace"}}>Hành Chính</div>
                    <div>Tăng Ca</div>
                </td>
                {ngayCong.bangCong?.map((item,index) => {
                    if(sunIndex.includes(index)){
                        return <td className='bg-red-300' style={{verticalAlign:"top",padding:"0"}}>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderBottom: "1px solid #119ace",borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.vao?item?.hanhChinh?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{borderBottom: "1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.ra}</div>
                                    </div>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.tangCa?.vao?item?.tangCa?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{padding:"2.5px 0"}}>{item?.tangCa?.ra}</div>
                                    </div>
                                </td>
                    }else if(satIndex.includes(index)){
                        return <td className='bg-yellow-300' style={{verticalAlign:"top",padding:"0"}}>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderBottom: "1px solid #119ace",borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.vao?item?.hanhChinh?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{borderBottom: "1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.ra}</div>
                                    </div>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.tangCa?.vao?item?.tangCa?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{padding:"2.5px 0"}}>{item?.tangCa?.ra}</div>
                                    </div>
                                </td>
                    }else{
                        return <td style={{verticalAlign:"top",padding:"0"}}>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderBottom: "1px solid #119ace",borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.vao?item?.hanhChinh?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{borderBottom: "1px solid #119ace",padding:"2.5px 0"}}>{item?.hanhChinh?.ra}</div>
                                    </div>
                                    <div className='grid grid-cols-2'>
                                        <div style={{borderRight:"1px solid #119ace",padding:"2.5px 0"}}>{item?.tangCa?.vao?item?.tangCa?.vao:<span className='text-transparent'>A</span>}</div>
                                        <div style={{padding:"2.5px 0"}}>{item?.tangCa?.ra}</div>
                                    </div>
                                </td>
                    }
                    
                })}
            </tr>
        })
    }
  return (
    <div id='ChamMay' className='px-1'>
        <div className='overflow-x-scroll'>
            <table className=''>
                <thead>
                    <th>TÊN NV</th>
                    <th style={{minWidth:"150px"}}>CA</th>
                    {renderDay()}
                </thead>
                <tbody className='text-center'>
                    {renderBangCong()}
                </tbody>
            </table>
        </div>
    </div>
  )
}
