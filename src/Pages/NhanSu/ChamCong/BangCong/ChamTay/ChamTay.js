import moment from 'moment/moment';
import React from 'react'
import { chamTay } from '../../../../../services/chamCongService';
import { Popover } from 'antd';

export default function ChamTay() {
    let sunIndex = [];
    let satIndex = [];
    const renderNote = (note) => {
        return <div>
            <input style={{border:"1px solid #d5d5d5",padding:"7px",fontSize:"15px"}} type="text" name="" id="" defaultValue={note} />
        </div>
    }
    let renderDay = () => {
        let month = "2023-09";
        // tính số ngày của 1 tháng
        let day = moment(month).daysInMonth();
        let array = [];
        for(let i = 1; i <= day; i++){
            if(i<10){
                i  = "0" + i;
                array.push(i);
            }else{
                array.push(String(i));
            }
        }
        return array.map((item,index) => {
            let date = moment(month + "-" + item).format('ddd');
            if(date == "Sun"){
                sunIndex.push(index);
                return <th className='text-red-500'>
                    <p>{item}</p>
                    <p>{date}</p>
                </th>
            }else if(date == "Sat"){
                satIndex.push(index);
                return <th className='text-yellow-500'>
                    <p>{item}</p>
                    <p>{date}</p>
                </th>
            }else{
                return <th>
                    <p>{item}</p>
                    <p>{date}</p>
                </th>
            }
            
        })
    }
    let renderBangCong = () => {
        return chamTay.map((ngayCong) => {
            return <tr>
                <td>{ngayCong.user_name}</td>
                {ngayCong.bangCong?.map((item,index) => {
                    if(sunIndex.includes(index)){
                        return <Popover content={() => renderNote(item.note)} placement='bottom' title="Ghi Chú: " trigger="hover"><td className='bg-red-300'>{item.work?item.work:""}</td></Popover>
                    }else if(satIndex.includes(index)){
                        return <Popover content={() => renderNote(item.note)} placement='bottom' title="Ghi Chú: " trigger="hover"><td className='bg-yellow-300'>{item.work?item.work:""}</td></Popover>
                    }else{
                        return <Popover content={() => renderNote(item.note)} placement='bottom' title="Ghi Chú: " trigger="hover"><td>{item.work?item.work:""}</td></Popover>
                    }
                })}
            </tr>
        })
    }
    return (
    <div id='ChamTay' className='px-1'>
        <div className='overflow-x-scroll'>
            <table className=''>
                <thead>
                    <th>TÊN NV</th>
                    {renderDay()}
                </thead>
                <tbody className='text-center'>
                    {renderBangCong()}
                </tbody>
            </table>
        </div>
    </div>
  )
}
