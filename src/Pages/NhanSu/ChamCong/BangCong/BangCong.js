import React from 'react'
import BangCongMenu from './BangCongMenu/BangCongMenu'
import ChamTay from './ChamTay/ChamTay'
import { useSelector } from 'react-redux'
import ChamMay from './ChamMay/ChamMay';

export default function BangCong() {
  let chamCong = useSelector((state) => state.chamCongSlice.chamCong);
  return (
    <div id='BangCong' className=''>
      <BangCongMenu></BangCongMenu>
      {chamCong==0&&<ChamTay></ChamTay>}
      {chamCong==1&&<ChamMay></ChamMay>}
    </div>
  )
}
