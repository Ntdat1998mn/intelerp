import moment from 'moment/moment'
import React, { useState } from 'react'
import { setChamCong } from '../../../../../reduxToolkit/reducer/chamCongSlice'
import { useDispatch } from 'react-redux'

export default function BangCongMenu() {
    let dispatch = useDispatch();
    let [month,setMonth] = useState(moment().format("YYYY-MM"))
    let changMonth = (e) => {
        setMonth(e.target.value);
    }
    let changeChamCong = (e) => {
        dispatch(setChamCong(e.target.value));
    }
  return (
    // xem file NhanSu.css
    <div id='BangCongMenu' className=''>
        <div className='flex justify-center'>
            <input onChange={changMonth} className='mx-3 text-sm font-medium' type="month" value={month} />
            <select onChange={changeChamCong} className='mx-3 text-sm font-medium' name="" id="">
                <option value="0">Chấm Tay</option>
                <option value="1">Chấm Máy</option>
            </select>
            <select className='mx-3 text-sm font-medium' name="" id="">
                <option value="">Full-Time</option>
                <option value="">Part-Time</option>
            </select>
            <button class="relative inline-flex items-center justify-center  mr-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-cyan-500 to-blue-500 group-hover:from-cyan-500 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-cyan-200 dark:focus:ring-cyan-800">
  <span class="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0"><i className="fa fa-map-marker-alt mr-1 text-blue-500"></i>
      Định Vị
  </span>
</button>
        </div>
        
    </div>
  )
}
