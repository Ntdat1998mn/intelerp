import React, { useState } from 'react'
import { nhanVienInfo } from '../../../../services/chamCongService'
import moment from 'moment/moment'
import { Tabs } from 'antd';
import iconEdit from "../../../../issets/img/icon/Báo cáo tổng-01.png";
import iconDownload from "../../../../issets/img/icon/Untitled-2-02.png"

export default function NhanVienDetail() {
  let [tab,setTab] = useState(1);
  let renderLeftDetail = () => {
    return <div className='w-full grid grid-cols-3'>
      <div className='self-center relative'>
        <img src={nhanVienInfo?.user_avatar} alt="" />
        <div className='w-full h-full absolute top-0 right-0 bg-black hover:opacity-70 opacity-0 flex items-center justify-center'>
            <div className='text-center'>
                <a href={nhanVienInfo?.user_avatar} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Xem</button></a><br />
              <button className='bg-red-500 text-white px-3 py-1 mt-2 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 left-0 opacity-0' />Đổi Ảnh</button>
            </div>
        </div>
      </div>
      <div style={{fontSize:"15px",height:"150px"}} className='col-span-2 ml-3 overflow-y-scroll'>
            <h2 className='text-center text-lg font-semibold text-blue-500'>{nhanVienInfo?.user_fullname}</h2>
            <p className='text-center'><u>Chức Vụ:</u> {nhanVienInfo?.user_chucVu}</p>
            <p className='mt-3'>Số CCCD: {nhanVienInfo?.user_ccccd}</p>
            <p>Ngày Sinh: {moment(nhanVienInfo?.user_birthday).format("DD/MM/YYYY")}</p>
            <p>Giới Tính: {nhanVienInfo?.user_gender}</p>
            <p>Trình Độ: {nhanVienInfo?.user_hocVan}</p>
            <p>SĐT: {nhanVienInfo?.user_phone}</p>
            <p>Ngày Vào Làm: {moment(nhanVienInfo?.user_start).format("DD/MM/YYYY")}</p>
      </div>
    </div>
  }
  let renderHopDong = () => {
    return <div style={{fontSize:"15px"}} className=''>
      <div className='text-center mb-3'>
          <a href={nhanVienInfo?.hopDong?.img} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Tải Về</button></a>
          <button className='bg-red-500 text-white px-3 py-1 ml-3 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 left-0 opacity-0' />Đổi File</button>
      </div>
      <div className='flex justify-center'>
        <div className="checkbox-wrapper-33">
          <label className="checkbox">
            <input defaultChecked={nhanVienInfo?.hopDong?.loaiHopDong==0} className="checkbox__trigger visuallyhidden" type="checkbox" />
            <span className="checkbox__symbol">
              <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 14l8 7L24 7"></path>
              </svg>
            </span>
            <p className="checkbox__textwrapper text-base">Không Thời Hạn</p>
          </label>
        </div>
        <div className="checkbox-wrapper-33 ml-5">
          <label className="checkbox">
            <input defaultChecked={nhanVienInfo?.hopDong?.loaiHopDong==1} className="checkbox__trigger visuallyhidden" type="checkbox" />
            <span className="checkbox__symbol">
              <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 14l8 7L24 7"></path>
              </svg>
            </span>
            <p className="checkbox__textwrapper text-base">Có Thời Hạn</p>
          </label>
        </div>
      </div>
      <p className='text-center mt-3'>Ngày Hết Hạn: <span className='font-semibold'>{moment(nhanVienInfo.hopDong.expiredDay).format("DD/MM/YYYY")}</span></p>
    </div>
  }
  let renderCCCD = () => {
    return <div className='grid grid-cols-3 gap-4'>
      <div className='relative'>
          <img src={nhanVienInfo?.cccd.cccd_matTruoc} alt="" />
          <div className='w-full h-full absolute top-0 right-0 bg-black hover:opacity-70 opacity-0 flex items-center justify-center'>
            <div className='text-center'>
                <a href={nhanVienInfo?.cccd.cccd_matTruoc} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Xem</button></a><br />
                <button className='bg-red-500 text-white px-3 py-1 mt-2 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 left-0 opacity-0' />Đổi Ảnh</button>
            </div>
        </div>
      </div>
      <div className='relative'>
        <img src={nhanVienInfo?.cccd.cccd_matSau}  />
        <div className='w-full h-full absolute top-0 right-0 bg-black hover:opacity-70 opacity-0 flex items-center justify-center'>
            <div className='text-center'>
                <a href={nhanVienInfo?.cccd.cccd_matSau} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Xem</button></a><br />
                <button className='bg-red-500 text-white px-3 py-1 mt-2 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 left-0 opacity-0' />Đổi Ảnh</button>
            </div>
        </div>
      </div>
      <div>
        <p className='font-semibold text-lg'>Hiệu Lực Đến: <span className='text-red-500'>{moment(nhanVienInfo?.cccd?.cccd_expired).format("DD/MM/YYYY")}</span></p>
      </div>
    </div>
  }
  let renderGPLX = () => {
    return <div className='grid grid-cols-3 gap-4'>
      <div className='relative'>
          <img src={nhanVienInfo?.gplx.gplx_matTruoc} alt="" />
          <div className='w-full h-full absolute top-0 right-0 bg-black hover:opacity-70 opacity-0 flex items-center justify-center'>
            <div className='text-center'>
                <a href={nhanVienInfo?.gplx.gplx_matTruoc} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Xem</button></a><br />
                <button className='bg-red-500 text-white px-3 py-1 mt-2 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 opacity-0' />Đổi Ảnh</button>
            </div>
        </div>
      </div>
      <div className='relative'>
        <img src={nhanVienInfo?.gplx.gplx_matSau}  />
        <div className='w-full h-full absolute top-0 right-0 bg-black hover:opacity-70 opacity-0 flex items-center justify-center'>
            <div className='text-center'>
                <a href={nhanVienInfo?.gplx.gplx_matSau} target="_blank" rel="noopener noreferrer"><button className='bg-green-500 text-white px-3 py-1 rounded'>Xem</button></a><br />
                <button className='bg-red-500 text-white px-3 py-1 mt-2 rounded relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 opacity-0' />Đổi Ảnh</button>
            </div>
        </div>
      </div>
      <div>
        <p className='font-semibold text-lg'>Hiệu Lực Đến: <span className='text-red-500'>{moment(nhanVienInfo?.gplx?.gplx_expired).format("DD/MM/YYYY")}</span></p>
      </div>
    </div>
  }
  let renderChungChi = () => {
    return <div style={{height:"100px"}} className='px-3 text-center overflow-y-scroll'>
      <table className='w-full'>
        <thead>
          <th>Tên Chứng Chỉ</th>
          <th>Ngày Cấp</th>
          <th>Thao Tác</th>
        </thead>
        <tbody>
          {nhanVienInfo?.chungChi?.map((chungChi) => {
            return <tr>
                      <td>{chungChi.chungChi_name}</td>
                      <td>{moment(chungChi?.chungChi_date).format("DD/MM/YYYY")}</td>
                      <td>
                        <button title='Tải Về'><a href={chungChi?.chungChi_file} target="_blank" rel="noopener noreferrer"><img width={"25px"} src={iconDownload} alt="" /></a></button>
                        <button width={"25px"} title='Đổi File' className='ml-5 relative'><input type="file" name="" id="" className='w-full absolute h-full top-0 left-0 opacity-0'/><img width={"25px"} src={iconEdit} alt="" /></button>
                      </td>
                    </tr>
          })}
          <tr>
            <td className='p-0'><input className='focus:outline-none pl-3' style={{width:"100%"}} type="text" placeholder='Thêm mới chứng chỉ...' /></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  }
  const items = [
    {
      key: '1',
      label: 'Hợp Đồng',
      children: renderHopDong(),
    },
    {
      key: '2',
      label: 'CCCD',
      children: renderCCCD(),
    },
    {
      key: '3',
      label: 'GPLX',
      children: renderGPLX(),
    },
    {
      key: '4',
      label: 'Chứng Chỉ',
      children: renderChungChi(),
    }
  ];
  let renderRightMenu = () => {
    return <div className='w-full'>
      <div>
        <Tabs type='card' defaultActiveKey='1' items={items}/>
      </div>
    </div>
  }
  let renderRightDetail = () => {
    return( 
      renderRightMenu()
    )
  }
  return (
    <div id ="nhanVienDetail" className='h-full'>
      <div className='flex w-full h-full'>
        <div style={{width:"45%"}}>{renderLeftDetail()}</div>
        <div style={{width:"55%"}}>{renderRightDetail()}</div>
      </div>
    </div>
  )
}
