import React from 'react'
import PhongBan from './PhongBan/PhongBan'
import BangCong from './BangCong/BangCong'
import NhanVienDetail from './NhanVienDetail/NhanVienDetail'

export default function ChamCong() {
  return (
    <div id='chamCong'>
      <div className='grid grid-cols-5'>
          <div className=''>
                <PhongBan></PhongBan>
          </div>
          <div className='col-span-4'>
            <div style={{height:"60%"}}  className='overflow-y-scroll relative'>
              <BangCong></BangCong>
            </div>
            <div style={{height:"40%"}}>
              <NhanVienDetail></NhanVienDetail>
            </div>
          </div>
      </div>
    </div>
  )
}
