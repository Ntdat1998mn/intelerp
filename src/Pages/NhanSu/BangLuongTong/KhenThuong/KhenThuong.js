import React, { useRef, useState } from 'react'
import { danhSachKhenThuong, khenThuong } from '../../../../services/bangLuongService'
import { Popconfirm } from 'antd'
import iconAdd from '../../../../issets/img/icon/ICON ADD.png'
import iconRemove from '../../../../issets/img/icon/ICON REMOVE.png'
import iconExcel from '../../../../issets/img/icon/excel.png'
import moment from 'moment/moment'
import { useDownloadExcel } from 'react-export-table-to-excel';
import SelectNhanVien from '../../GlobalComp/SelectNhanVien'

export default function KhenThuong(props) {
    let [showAdd,setShowAdd] = useState(true);
    let tong = 0;
    const tableRef = useRef(null);
    const { onDownload } = useDownloadExcel({
        currentTableRef: tableRef.current,
        filename: `thong-ke-khen-thuong-${props.currentMonth}`,
        sheet: 'KhenThuong'
    })
    let handleShowAddNhanVien = () => {
        setShowAdd(false);
    }
    let renderNoiQuy = () => {
        return khenThuong.map((item,index) => {
            return <tr>
                <td><Popconfirm title="Xoá Nội Quy"><img className='mx-auto cursor-pointer' width={"30px"} src={iconRemove} alt="" /></Popconfirm></td>
                <td><input className='focus:outline-none text-center w-full' type="text" defaultValue={item.noiquy_name}/></td>
                <td><input className='focus:outline-none text-center w-full' type="text" defaultValue={new Intl.NumberFormat("EN-US").format(item.noiquy_price)}/></td>
            </tr>
        })
    }
    let renderSelectNoiQuy = (id) => {
        return khenThuong.map((item,index) => {
            if(item.noiquy_id == id){
                return <option selected value={item.noiquy_id}>{item.noiquy_name}</option>
            }else{
                return <option value={item.noiquy_id}>{item.noiquy_name}</option>
            }
        })
    }
    let renderNhanVien = () => {
        return danhSachKhenThuong.map((nhanVien,index) => {
            tong += Number(nhanVien.khenthuong_soTien);
            return <tr>
                <td><Popconfirm title="Xoá Nhân Viên"><img className='mx-auto cursor-pointer' width={"30px"} src={iconRemove} alt="" /></Popconfirm></td>
                <td>
                    {moment(props.currentMonth).format("MM/YYYY")}
                </td>
                <td>
                    {nhanVien.khenthuong_name}
                </td>
                <td>
                    <select placeholder='Chọn Mục Khen Thưởng' className='focus:outline-none w-full text-center' name="" id="">
                        {renderSelectNoiQuy(nhanVien?.khenthuong_noiQuy)}
                    </select>
                </td>
                <td>
                    {new Intl.NumberFormat("EN-US").format(nhanVien.khenthuong_soTien)}
                </td>
            </tr>
        })
    }
    let renderNoiQuyExcel = (id) => {
        let index = khenThuong.findIndex((item) => {
            return item.noiquy_id == id;
        })
        return khenThuong[index].noiquy_name;
    }
    let renderNhanVienExcel = () => {
        return danhSachKhenThuong.map((nhanVien,index) => {
            return <tr>
                <td>{index+1}</td>
                <td>
                    {moment(props.currentMonth).format("MM/YYYY")}
                </td>
                <td>
                    {nhanVien.khenthuong_name}
                </td>
                <td>
                    {renderNoiQuyExcel(nhanVien.khenthuong_noiQuy)}
                </td>
                <td>
                    {new Intl.NumberFormat("EN-US").format(nhanVien.khenthuong_soTien)}
                </td>
            </tr>
        })
    }
  return (
    <div id='khenThuong'>
        <div className='my-3'>
                   <button onClick={onDownload} style={{height:"100%",border:"solid 1px #01b4e2",borderRadius:"5px", padding:"5px 9px"}} className='flex items-center'><img style={{width:"30px"}} src={iconExcel} alt="" /><span className='ml-2 font-semibold text-base'>Xuất Báo Cáo</span></button>
        </div>
        <div className='grid grid-cols-3 gap-5'>
            <div>
                <table className='w-full text-center'>
                    <thead className='leading-8'>
                        <th style={{width:"40px"}}></th>
                        <th>Khen Thưởng</th>
                        <th>Số Tiền</th>
                    </thead>
                    <tbody>
                        {renderNoiQuy()}
                        <tr>
                            <td></td>
                            <td><input className='focus:outline-none pl-2 w-full ' type="text" placeholder='Thêm Nội Quy Mới...' /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className='col-span-2'>
                <table className='w-full text-center'>
                    <thead className='leading-8'>
                        <th style={{width:"40px"}}><img onClick={handleShowAddNhanVien} className='mx-auto cursor-pointer' width={"30px"} src={iconAdd} alt="" /></th>
                        <th>Tháng</th>
                        <th>Nhân Viên</th>
                        <th>Khen Thưởng</th>
                        <th>Số Tiền</th>
                    </thead>
                    <tbody>
                        {renderNhanVien()}
                        <tr className={`${showAdd&&"hidden"}`}>
                            <td></td>
                            <td></td>
                            <td>
                                <SelectNhanVien></SelectNhanVien>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tổng:</td>
                            <td>{new Intl.NumberFormat("EN-US").format(tong)}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {/* bảng này để xuất excel */}
        <div className='hidden'>
                <table className='w-full text-center' ref={tableRef}>
                    <thead className='leading-8'>
                        <th style={{width:"40px"}}>STT</th>
                        <th>Tháng</th>
                        <th>Nhân Viên</th>
                        <th>Khen Thưởng</th>
                        <th>Số Tiền</th>
                    </thead>
                    <tbody>
                        {renderNhanVienExcel()}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tổng:</td>
                            <td>{new Intl.NumberFormat("EN-US").format(tong)}</td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
  )
}
