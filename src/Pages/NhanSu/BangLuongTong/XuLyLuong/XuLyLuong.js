import { Switch } from 'antd'
import React from 'react'

export default function XuLyLuong() {
    let changeheader = (e) => {
        console.log(e.target.checked);
    }
  return (
    <div id='xuLyLuong'>
        <h2 className='text-center text-lg font-semibold my-3'>ĐIỀU CHỈNH BẢNG LƯƠNG</h2>
        <div className='grid grid-cols-3 px-3 gap-5 text-center'>
            <div>
                <table className='w-full'>
                    <tbody>
                        <tr>
                            <td>Phụ Cấp Trách Nhiệm</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCTN' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Phụ Cấp Cơm</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCTC' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Phụ Cấp Xăng</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCX' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Phụ Cấp Điện Thoại</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCDT' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table className='w-full'>
                    <tbody>
                        <tr>
                            <td>Phụ Cấp Thêm Giờ</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCTG' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Phụ Cấp Chuyên Cần</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='PCCC' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>BHXH</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='BHXH' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Thưởng</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='thuong' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table className='w-full'>
                    <tbody>
                        <tr>
                            <td>Vi Phạm</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='viPham' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Thưởng KPI</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='KPI' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Tạm Ứng</td>
                            <td>
                                <div className="checkbox-wrapper-33 flex justify-center ml-3">
                                    <label className="checkbox">
                                        <input onChange={changeheader} id='tamUng' className="checkbox__trigger visuallyhidden" type="checkbox" defaultChecked={true} />
                                        <span className="checkbox__symbol">
                                        <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4 14l8 7L24 7"></path>
                                        </svg>
                                        </span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  )
}
