import React, { useState } from 'react'
import PhongBan from '../ChamCong/PhongBan/PhongBan'
import moment from 'moment/moment'
import LuongChinh from './LuongChinh/LuongChinh'
import ViPham from './ViPham/ViPham';
import KhenThuong from './KhenThuong/KhenThuong';
import UngLuong from './UngLuong/UngLuong';
import TangGiamLuong from './TangGiamLuong/TangGiamLuong';
import XuLyLuong from './XuLyLuong/XuLyLuong';

export default function BangLuongTong() {
  let [month,setMonth] = useState(moment().format("YYYY-MM"));
  let [tab,setTab] = useState(1);
  let changMonth = (e) => {
    setMonth(e.target.value);
  }
  let renderTab = () => {
      switch (tab) {
        case "1": return <LuongChinh></LuongChinh>;
        case "2": return <ViPham currentMonth={month}></ViPham>;
        case "3": return <KhenThuong currentMonth={month}></KhenThuong>;
        case "4": return <UngLuong currentMonth={month}></UngLuong>;
        case "5": return <TangGiamLuong currentMonth={month}></TangGiamLuong>;
        case "6": return <XuLyLuong currentMonth={month}></XuLyLuong>;
      
        default: return <LuongChinh></LuongChinh>;
      }
  }
  return (
    <div id='bangLuongTong'>
      <div className='grid grid-cols-5'>
          <div className=''>
                <PhongBan></PhongBan>
          </div>
          <div className='col-span-4'>
            <div id='LuongTongMenu' className='flex justify-center'>
              <input onChange={changMonth} className='mx-3 text-sm font-medium' type="month" value={month} />
              <select className='mx-3 text-sm font-medium' name="" id="">
                <option value="">Full-Time</option>
                <option value="">Part-Time</option>
              </select>
              <select onChange={(e) => {setTab(e.target.value)}} className='mx-3 text-sm font-medium' name="" id="">
                <option value="1">Lương Chính</option>
                <option value="2">Vi Phạm</option>
                <option value="3">Thưởng</option>
                <option value="4">Ứng Lương</option>
                <option value="5">Tăng/Giảm Lương</option>
                <option value="6">Xử Lý Lương</option>
              </select>
            </div>
            <div className='bangLuongTab'>
              {renderTab()}
            </div>
          </div>
      </div>
    </div>
  )
}
