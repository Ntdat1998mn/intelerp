import React, { useState } from 'react'
import iconAdd from '../../../../issets/img/icon/ICON ADD.png'
import iconRemove from '../../../../issets/img/icon/ICON REMOVE.png'
import iconExcel from '../../../../issets/img/icon/excel.png'
import moment from 'moment/moment'
import SelectNhanVien from '../../GlobalComp/SelectNhanVien'
import { tangGiamLuong } from '../../../../services/bangLuongService'
import { Popconfirm } from 'antd'

export default function TangGiamLuong(props) {
    let [showAdd,setShowAdd] = useState(true);
    let handleShowAddNhanVien = () => {
        setShowAdd(false);
    }
    let renderTangGiamLuong = () => {
        return tangGiamLuong.map((item,index) => {
            return <tr>
                <td><Popconfirm title="Xoá Nhân Viên"><img className='mx-auto cursor-pointer' width={"30px"} src={iconRemove} alt="" /></Popconfirm></td>
                <td>{moment(props.currentMonth).format("MM/YYYY")}</td>
                <td>{item.tanggiam_name}</td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat("EN-US").format(item.tanggiam_lcb)} /></td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat("EN-US").format(item.tanggiam_tang)} /></td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat("EN-US").format(item.tanggiam_giam)} /></td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={item.tanggiam_ghichu} /></td>
            </tr>
        })
    }
  return (
    <div id='tangGiamLuong'>
        <table className='w-full text-center'>
                <thead className='leading-8'>
                    <th style={{width:"40px"}}><img onClick={handleShowAddNhanVien} className='mx-auto cursor-pointer' width={"30px"} src={iconAdd} alt="" /></th>
                    <th>Tháng</th>
                    <th>Nhân Viên</th>
                    <th>Lương Cơ Bản</th>
                    <th>Tăng Lương</th>
                    <th>Giảm Lương</th>
                    <th>Ghi Chú</th>
                </thead>
                <tbody>
                        {renderTangGiamLuong()}
                        <tr className={`${showAdd&&"hidden"}`}>
                            <td></td>
                            <td>Thêm nhân viên mới...</td>
                            <td><SelectNhanVien></SelectNhanVien></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
    </div>
  )
}
