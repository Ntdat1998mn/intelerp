import React, { useRef, useState } from 'react'
import { ungLuong} from '../../../../services/bangLuongService'
import iconAdd from '../../../../issets/img/icon/ICON ADD.png'
import iconRemove from '../../../../issets/img/icon/ICON REMOVE.png'
import iconExcel from '../../../../issets/img/icon/excel.png'
import moment from 'moment/moment'
import { Modal, Popconfirm } from 'antd'
import SelectNhanVien from '../../GlobalComp/SelectNhanVien'
import { useReactToPrint } from 'react-to-print'
import { useDownloadExcel } from 'react-export-table-to-excel';

export default function UngLuong(props) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    let [nhanVien,setNhanVien] = useState();
    const componentRef = useRef();
    const tableRef = useRef(null);
    let [showAdd,setShowAdd] = useState(true);
    let tong = 0;
    const { onDownload } = useDownloadExcel({
        currentTableRef: tableRef.current,
        filename: `danh-sach-ung-luong-${props.currentMonth}`,
        sheet: 'UngLuong'
    })
    const showModal = (index) => {
        setIsModalOpen(true);
        setNhanVien(ungLuong[index]);
      };
    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
    });
    const handleOk = () => {
        setIsModalOpen(false);
        handlePrint()
      };
    
      const handleCancel = () => {
        setIsModalOpen(false);
      };
    let handleShowAddNhanVien = () => {
        setShowAdd(false);
    }
    let renderUngLuong = () => {
        return ungLuong.map((item,index) => {
            tong += item.ungluong_tien;
            return <tr>
                <td><Popconfirm title="Xoá Nhân Viên"><img className='mx-auto cursor-pointer' width={"30px"} src={iconRemove} alt="" /></Popconfirm></td>
                <td>{moment(props.currentMonth).format("MM/YYYY")}</td>
                <td>{item.ungluong_nhanvien}</td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={item.ungluong_ghichu} /></td>
                <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat("EN-US").format(item.ungluong_tien)} /></td>
                <td>
                    <button  onClick={() => showModal(index)} className='bg-green-500 text-white px-3 py-1 rounded font-semibold'>IN</button>
                </td>
            </tr>
        })
    }
    let renderUngLuongExcel = () => {
        return ungLuong.map((item,index) => {
            return <tr>
                <td>{index+1}</td>
                <td>{moment(props.currentMonth).format("MM/YYYY")}</td>
                <td>{item.ungluong_nhanvien}</td>
                <td>{item.ungluong_ghichu}</td>
                <td>{new Intl.NumberFormat("EN-US").format(item.ungluong_tien)}</td>
            </tr>
        })
    }
  return (
    <div id='ungLuong'>
        <div>
            <div className='my-3'>
                   <button onClick={onDownload} style={{height:"100%",border:"solid 1px #01b4e2",borderRadius:"5px", padding:"5px 9px"}} className='flex items-center'><img style={{width:"30px"}} src={iconExcel} alt="" /><span className='ml-2 font-semibold text-base'>Xuất Báo Cáo</span></button>
            </div>
            <table className='w-full text-center'>
                <thead className='leading-8'>
                    <th style={{width:"40px"}}><img onClick={handleShowAddNhanVien} className='mx-auto cursor-pointer' width={"30px"} src={iconAdd} alt="" /></th>
                    <th>Tháng</th>
                    <th>Nhân Viên</th>
                    <th>Ghi Chú</th>
                    <th>Số Tiền</th>
                    <th>Phiếu</th>
                </thead>
                <tbody>
                        {renderUngLuong()}
                        <tr className={`${showAdd&&"hidden"}`}>
                            <td></td>
                            <td>Thêm nhân viên mới...</td>
                            <td><SelectNhanVien></SelectNhanVien></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tổng:</td>
                            <td>{new Intl.NumberFormat("EN-US").format(tong)}</td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
            {/* Bảng này để xuất excel */}
            <div className='hidden'>
                <table className='w-full text-center' ref={tableRef}>
                    <thead className='leading-8'>
                        <th style={{width:"40px"}}></th>
                        <th>Tháng</th>
                        <th>Nhân Viên</th>
                        <th>Ghi Chú</th>
                        <th>Số Tiền</th>
                    </thead>
                    <tbody>
                            {renderUngLuongExcel()}
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Tổng:</td>
                                <td>{new Intl.NumberFormat("EN-US").format(tong)}</td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <Modal title="Phiếu Lương Nhân Viên" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
            <div ref={componentRef} className='p-3'>
                <br />
                <p className='font-semibold'>ĐƠN VỊ: <input type="text" className='pl-2'/></p>
                <p className='font-semibold'>ĐỊA CHỈ: <input type="text" className='pl-2'/></p>
                <br />
                <h2 className='text-center font-bold text-lg'>PHIẾU ỨNG LƯƠNG</h2>
                <p className='text-center text-sm'><input type="text" className=' pl-2 w-fit' defaultValue={"Ngày: " + moment().format("DD/MM/YYYY")} /></p>
                <br />
                <p className='text-base'>Nhân Viên/Bộ Phận: <input className='pl-2 font-semibold' type="text" placeholder='Nhập bộ phận...' /> </p>
                <br />
                <p className='text-base'>Địa Chỉ: <input className='pl-2 font-semibold' type="text" placeholder='Nhập địa chỉ...' /> </p>
                <p className='text-base'>Số Điện Thoại: <input className='pl-2 font-semibold' type="text" placeholder='Nhập số điện thoại...' /> </p>
                <p className='text-base'>Xác nhận đã ứng lương tháng {moment(props.currentMonth).format("MM/YYYY")} đối với anh/chị: <span className='font-semibold uppercase'>{nhanVien?.ungluong_nhanvien}</span></p>
                <p className='text-base'>Số Tiền: <span className='font-semibold uppercase'>{new Intl.NumberFormat("EN-US").format(nhanVien?.ungluong_tien)}đ</span> - viết bằng chữ: <input className='pl-2 font-semibold' type="text" placeholder='Nhập số tiền...' /></p>
                <p className='text-base'>Lý do: <span className='font-semibold uppercase'>{nhanVien?.ungluong_ghichu}</span></p>
                <br />
                <div className='grid grid-cols-2 text-center text-base'>
                    <div>
                        <p>Người Giao</p>
                        <br />
                        <br />
                        <br />
                    </div>
                    <div>
                        <p>Người Nhận</p>
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
      </Modal>
    </div>
  )
}
