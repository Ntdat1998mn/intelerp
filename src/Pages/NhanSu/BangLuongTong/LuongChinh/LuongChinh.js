import React, { useState } from 'react'
import iconAdd from '../../../../issets/img/icon/ICON ADD.png'
import iconRemove from '../../../../issets/img/icon/ICON REMOVE.png'
import { danhSachLuong } from '../../../../services/bangLuongService'
import { Popconfirm, Tooltip } from 'antd'
import SelectNhanVien from '../../GlobalComp/SelectNhanVien'

export default function LuongChinh() {
  let [showAdd,setShowAdd] = useState(true);
  let handerAddUser = () => {
    setShowAdd(false);
  }
  let renderBangLuong = () => {
    return danhSachLuong.map((nhanVien,item) => {
      return <tr>
        <td>{item+1}</td>
        <td>
          <div className="checkbox-wrapper-33 flex justify-center ml-3">
          <label className="checkbox">
            <input className="checkbox__trigger visuallyhidden" type="checkbox" />
            <span className="checkbox__symbol">
              <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 14l8 7L24 7"></path>
              </svg>
            </span>
          </label>
          </div>
        </td>
        <td><input className='w-full focus:outline-none text-center' type="text" defaultValue={nhanVien.bangluong_name}/></td>
        <td style={{width:"120px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_lcb) } name="" id="" /></td>
        <td style={{width:"100px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={nhanVien.bangluong_gioCong} name="" id="" /></td>
        <td style={{width:"110px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_pctn) } name="" id="" /></td>
        <td style={{width:"110px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_pcxang) } name="" id="" /></td>
        <td style={{width:"110px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_pccom) } name="" id="" /></td>
        <td style={{width:"100px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_pcdt) } name="" id="" /></td>
        <td style={{width:"140px"}}><input className='w-full focus:outline-none text-center' type="text" defaultValue={new Intl.NumberFormat('EN-US').format(nhanVien.bangluong_pcthemGio) } name="" id="" /></td>
      </tr>
    })
  }
  return (
    <div id='luongChinh' className='bg-white my-4'>
        <table className='w-full text-center'>
            <thead className='leading-8'>
                <th style={{maxWidth:"40px"}}><img onClick={handerAddUser} className='mx-auto cursor-pointer' width={"30px"} src={iconAdd} alt="" /></th>
                <th style={{maxWidth:"40px"}}><Popconfirm title="Xoá nhân viên" description="Bạn Chắc Chắn Xoá (Các) Nhân Viên này?"><img className='mx-auto cursor-pointer' width={"30px"} src={iconRemove} alt="" /></Popconfirm></th>
                <th>Nhân Viên</th>
                <th>Lương Cơ Bản</th>
                <th>Giờ Công</th>
                <th>Phụ Cấp TN</th>
                <th>Phụ Cấp Xăng</th>
                <th>Phụ Cấp Cơm</th>
                <th>Phụ Cấp ĐT</th>
                <th>Phụ Cấp Thêm Giờ</th>
            </thead>
            <tbody>
                {renderBangLuong()}
                <tr className={`${showAdd&&"hidden"}`}>
                  <td></td>
                  <td></td>
                  <td>
                    <SelectNhanVien></SelectNhanVien>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}
