import React, { useState } from 'react'
import { taoCongThuc } from '../../../../services/TaoBangLuongService'
import { Modal, Popconfirm, Switch } from 'antd'
import moment from 'moment/moment';

export default function TaoCongThuc() {
    let [month,setMonth] = useState(moment().format("YYYY-MM"));
    let changMonth = (e) => {
        setMonth(e.target.value);
    }
    const [open, setOpen] = useState(false);
    const showModal = () => {
        setOpen(true);
    };
    const handleCancel = () => {
        setOpen(false);
    };
    let renderCongThuc = () => {
        return taoCongThuc.map((congThuc,index) => {
            return <tr>
                <td>{index + 1}</td>
                <td>{congThuc.bangLuong_name}</td>
                <td>
                    <Switch style={{backgroundColor: "gray"}} defaultChecked={congThuc.bangLuong_status}></Switch>
                </td>
                <td>
                    <div className="checkbox-wrapper-33 flex justify-center pl-2">
                        <label className="checkbox">
                            <input defaultChecked={congThuc?.bangLuong_type==0} className="checkbox__trigger visuallyhidden" type="checkbox" />
                            <span className="checkbox__symbol">
                                <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4 14l8 7L24 7"></path>
                                </svg>
                            </span>
                        </label>
                    </div>
                </td>
                <td>
                    <input className='w-full focus:outline-none' type="text" defaultValue={congThuc.bangLuong_type==0?congThuc.bangLuong_number:congThuc.bangLuong_congThuc} disabled={congThuc.bangLuong_type==0} />
                </td>
                <td>
                    <div className="checkbox-wrapper-33 flex justify-center pl-2">
                        <label className="checkbox">
                            <input defaultChecked={congThuc?.bangLuong_all == 1} className="checkbox__trigger visuallyhidden" type="checkbox" />
                            <span className="checkbox__symbol">
                                <svg aria-hidden="true" className="icon-checkbox" width="28px" height="28px" viewBox="0 0 28 28" version="1" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4 14l8 7L24 7"></path>
                                </svg>
                            </span>
                        </label>
                    </div>
                </td>
            </tr>
        })
    }
    return (
    <div id='taoCongThuc' className='px-2'>
        <div className='w-9/12 mx-auto'>
            <input style={{border:"solid 1px #01b4e2"}} onChange={changMonth} className='my-5 text-base font-medium p-2 rounded' type="month" value={month} />
            <table className='w-full text-center'>
                <thead className='leading-8'>
                    <th>STT</th>
                    <th>Tên Loại Lương</th>
                    <th>Sử Dụng</th>
                    <th>Nhập Số</th>
                    <th>Công Thức</th>
                    <th>Áp Dụng Cho Toàn Công Ty</th>
                </thead>
                <tbody>
                    {renderCongThuc()}
                    <tr>
                        <td></td>
                        <td><input className='w-full focus:outline-none px-3' type="text" name="" id="" placeholder='Thêm mới ...' /></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <div className='text-right my-3'>
                <button onClick={showModal} className='bg-green-500 text-white text-base px-3 py-2 rounded mr-3'>Hướng Dẫn Tạo Công Thức</button>
                <Popconfirm title="Copy Dữ Liệu Tháng Cũ" description="Xác nhận copy công thức lương tháng cũ cho tháng hiện tại?" okText="Đồng Ý" cancelText="Huỷ"><button className='bg-red-500 text-white text-base px-3 py-2 rounded'>Copy Dữ Liệu Tháng Cũ</button></Popconfirm>
            </div>
        </div>
        <Modal
        open={open}
        onCancel={handleCancel}
        title={<p className='text-lg'>Tạo Công Thức</p>}
        footer={[]}
      >
        <div className='text-base'>
            <p><span style={{color:"red",fontWeight: "600"}}>**1.</span> Công thức chỉ được áp dụng khi: Bỏ dấu tích ở ô "Nhập số".</p>
            <p><span style={{color:"red",fontWeight: "600"}}>**2.</span> Công thức có thể bao gồm: số, phép tính (cộng [+], trừ [-], nhân [*], chia [/]), "Ký hiệu thông số", ngoặc ().</p>
            <p><span style={{color:"red",fontWeight: "600"}}>**3.</span>  "Ký hiệu thông số": Nguyên tắc viết
                        
                            </p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;- Sử dụng @ khi muốn gọi tên ký hiệu VD: @LCB@ * 20, @UL@ / 30</p>
            <p style={{lineHeight:"1.5"}}>&nbsp;&nbsp;&nbsp;&nbsp;- Ký hiệu trong @ không được viết "Cách Khoảng" VD: 
                                <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @ LCB @ &nbsp;<span style={{color:"red"}}>sai</span>
                                <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @LCB @ &nbsp;&nbsp;<span style={{color:"red"}}>sai</span>
                                <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @LCB@ &nbsp;&nbsp;&nbsp;<span style={{color:"red"}}>đúng</span>
                            </p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;- Ký hiệu sử dụng phải tồn tại trong bảng thông số cơ bản.</p>
            <p><span style={{color:"red",fontWeight: "600"}}>**4.</span> Lưu ý khi sử dụng dấu ngoặc: khi mở ngoặc cần phải đóng ngoặc.</p>
            <p><span style={{color:"red",fontWeight: "600"}}>**5.</span> Màu xanh thế hiện cho công thức hợp lệ. Màu đỏ thể hiện công thức nhập sai và giá trị sẽ là 0 trong bảng xử lý lương.</p>
        </div>
      </Modal>
    </div>
  )
}
