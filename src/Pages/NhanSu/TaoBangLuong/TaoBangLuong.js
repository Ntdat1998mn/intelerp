import { Tabs } from 'antd'
import React from 'react'
import BangThongSo from './BangThongSo/BangThongSo';
import TaoCongThuc from './TaoCongThuc/TaoCongThuc';
import CauHinh from './CauHinh/CauHinh';

export default function TaoBangLuong() {
  const items = [
    {
      key: '1',
      label: 'Bảng Thông Số',
      children: <BangThongSo></BangThongSo>,
    },
    {
      key: '2',
      label: 'Tạo Công Thức Lương',
      children: <TaoCongThuc></TaoCongThuc>,
    },
    {
      key: '3',
      label: 'Cấu Hình',
      children: <CauHinh></CauHinh>,
    }
  ];
  return (
    <div id='taoBangLuong' className='p-2'>
        <Tabs type='card' defaultActiveKey='1' items={items}> </Tabs>
    </div>
  )
}
