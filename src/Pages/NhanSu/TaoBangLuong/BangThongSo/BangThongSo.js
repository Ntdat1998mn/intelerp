import React from 'react'
import { bangThongSo } from '../../../../services/TaoBangLuongService'

export default function BangThongSo() {
    let renderTable1 = () => {
        return bangThongSo.map((bts,index) => {
            if(index < 7){
                return <tr>
                    <td>{index + 1}</td>
                    <td>{bts.thongSo_name}</td>
                    <td>{bts.thongSo_kyHieu}</td>
                </tr>
            }
        })
    }
    let renderTable2 = () => {
        return bangThongSo.map((bts,index) => {
            if(7 <= index && index < 15){
                return <tr>
                    <td>{index + 1}</td>
                    <td>{bts.thongSo_name}</td>
                    <td>{bts.thongSo_kyHieu}</td>
                </tr>
            }
        })
    }
    let renderTable3 = () => {
        return bangThongSo.map((bts,index) => {
            if(index >= 15){
                return <tr>
                    <td>{index + 1}</td>
                    <td>{bts.thongSo_name}</td>
                    <td>{bts.thongSo_kyHieu}</td>
                </tr>
            }
        })
    }
  return (
    <div id='bangThongSo' className='grid grid-cols-3 text-center'>
        <div>
            <table className='w-10/12 mx-auto'>
                <thead className='leading-8'>
                    <th>STT</th>
                    <th>Thông Số</th>
                    <th>Ký Hiệu</th>
                </thead>
                <tbody>
                    {renderTable1()}
                </tbody>
            </table>
        </div>
        <div>
            <table className='w-10/12 mx-auto'>
                <tbody>
                    {renderTable2()}
                </tbody>
            </table>
        </div>
        <div>
            <table className='w-10/12 mx-auto'>
                <tbody>
                    {renderTable3()}
                </tbody>
            </table>
        </div>
    </div>
  )
}
