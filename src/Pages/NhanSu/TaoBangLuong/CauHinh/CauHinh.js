import React from 'react'
import { cauHinh, gioCongChuan, gioCongChuanParttime, thoiGianCaLamViec, thoiGianParttime } from '../../../../services/TaoBangLuongService'
import { Switch } from 'antd'

export default function CauHinh() {
  let renderCauHinh = () => {
    return cauHinh.map((item,index) => {
      return <tr>
        <td>{index+1}</td>
        <td>{item.danhMuc_name}</td>
        <td>
          <Switch style={{backgroundColor: "gray"}} defaultChecked={item.danhMuc_status==1}></Switch>
        </td>
      </tr>
    })
  }
  let renderGioVaoCa = () => {
    return<>
          <tr>
            <td>Giờ Vào</td>
            <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianCaLamViec.hanhChinh.gioVao} /></td>
          </tr>
          <tr>
            <td>Giờ Ra</td>
            <td>
              <input className=' focus:outline-none' type="time" defaultValue={thoiGianCaLamViec.hanhChinh.gioRa} />
            </td>
          </tr>
          <tr>
            <td>Vào Tăng Ca</td>
            <td>
              <input className=' focus:outline-none' type="time" defaultValue={thoiGianCaLamViec.tangCa.gioVao} />
            </td>
          </tr>
          <tr>
            <td>Ra Tăng Ca</td>
            <td>
              <input className=' focus:outline-none' type="time" defaultValue={thoiGianCaLamViec.tangCa.gioRa} />
            </td>
          </tr>
    </> 
  }
  let renderGioCongChuan = () => {
    return <>
      <tr>
        <td>Hành Chính</td>
        <td><input style={{width:"25px",textAlign:"center",border:"solid 1px #01b4e2"}} className='focus:outline-none rounded-md' type="text" name="" id="" defaultValue={gioCongChuan.hanhChinh} /> Giờ/Ngày</td>
      </tr>
      <tr>
        <td>Tăng Ca</td>
        <td><input style={{width:"25px",textAlign:"center",border:"solid 1px #01b4e2"}} className='focus:outline-none rounded-md' type="text" name="" id="" defaultValue={gioCongChuan.tangCa} /> Giờ/Ngày</td>
      </tr>
    </>
  }
  let renderThoiGianParttime = () => {
    return <>
      <tr>
        <td>Ca 1: Vào</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca1.gioVao} /></td>
      </tr>
      <tr>
        <td>Ca 1: Ra</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca1.gioRa} /></td>
      </tr>
      <tr>
        <td>Ca 2: Vào</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca2.gioVao} /></td>
      </tr>
      <tr>
        <td>Ca 2: Ra</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca2.gioRa} /></td>
      </tr>
      <tr>
        <td>Ca 3: Vào</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca3.gioVao} /></td>
      </tr>
      <tr>
        <td>Ca 3: Ra</td>
        <td><input className=' focus:outline-none' type="time" defaultValue={thoiGianParttime.ca3.gioRa} /></td>
      </tr>
    </>
  }
  let renderGioCongChuanParttime = () => {
    return <>
      <tr>
        <td>Ca 1: </td>
        <td><input style={{width:"25px",textAlign:"center",border:"solid 1px #01b4e2"}} className='focus:outline-none rounded-md' type="text" name="" id="" defaultValue={gioCongChuanParttime.ca1} /> Giờ/Ngày</td>
      </tr>
      <tr>
        <td>Ca 2: </td>
        <td><input style={{width:"25px",textAlign:"center",border:"solid 1px #01b4e2"}} className='focus:outline-none rounded-md' type="text" name="" id="" defaultValue={gioCongChuanParttime.ca2} /> Giờ/Ngày</td>
      </tr>
      <tr>
        <td>Ca 3: </td>
        <td><input style={{width:"25px",textAlign:"center",border:"solid 1px #01b4e2"}} className='focus:outline-none rounded-md' type="text" name="" id="" defaultValue={gioCongChuanParttime.ca3} /> Giờ/Ngày</td>
      </tr>
    </>
  }
  return (
    <div id='cauHinh'>
      <div style={{height:"400px"}} className='grid grid-cols-3 text-center gap-5 overflow-y-scroll h-screen'>
        <div>
          <h2 className='my-3 text-lg font-semibold'>Cấu Hình Chức Năng</h2>
          <table className='w-full'>
            <thead className='leading-8'>
              <th>STT</th>
              <th>Chức Năng</th>
              <th>Sử Dụng</th>
            </thead>
            <tbody>
              {renderCauHinh()}
            </tbody>
          </table>
        </div>
        <div>
          <h2 className='my-3 text-lg font-semibold'>Cấu Hình Máy Chấm Công</h2>
          <table className='w-full'>
            <thead className='leading-8'>
              <th>Hành Chính</th>
              <th>Thời Gian</th>
            </thead>
            <tbody>
              {renderGioVaoCa()}
            </tbody>
          </table>
          <table className='w-full mt-8'>
            <thead className='leading-8'>
                <th>Khung Giờ</th>
                <th>Giờ Công Chuẩn</th>
              </thead>
              <tbody>
                {renderGioCongChuan()}
              </tbody>
            </table>
        </div>
        <div>
        <h2 className='my-3 text-lg font-semibold text-transparent'>A</h2>
          <table className='w-full'>
            <thead className='leading-8'>
              <th>Theo Ca</th>
              <th>Thời Gian</th>
            </thead>
            <tbody>
              {renderThoiGianParttime()}
            </tbody>
          </table>
          <table className='w-full my-8'>
            <thead className='leading-8'>
                <th>Khung Giờ</th>
                <th>Giờ Công Chuẩn</th>
              </thead>
              <tbody>
                {renderGioCongChuanParttime()}
              </tbody>
            </table>
        </div>
      </div>
    </div>
  )
}
