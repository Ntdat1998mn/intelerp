import React, { useState } from 'react'
import Select from 'react-select'

export default function SelectNhanVien(props) {
    let option = [];
    let [userList,setUserList] = useState([
        {
            user_id: 1,
            user_name: "Nguyễn Văn A"
        },
        {
            user_id: 2,
            user_name: "Nguyễn Văn B"
        },
        {
            user_id: 3,
            user_name: "Nguyễn Văn C"
        },
        {
            user_id: 4,
            user_name: "Nguyễn Văn D"
        }
    ]);
    let renderOption = () => {
        let array = [];
        userList.map((user,index) => {
            let item = {
                value: user.user_id,
                label: user.user_name
            }
            array.push(item);
        })
        option = [...array];
    }
  return (
    <div>
        {renderOption()}
        <Select placeholder="Chọn Nhân Viên" options={option}>
        </Select>
    </div>
  )
}
