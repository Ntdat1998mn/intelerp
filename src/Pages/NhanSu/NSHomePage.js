import React, { useState } from 'react'
import "./NhanSu.css"
import ChamCong from "./ChamCong/ChamCong"
import TaoBangLuong from "./TaoBangLuong/TaoBangLuong"
import BangLuongTong from './BangLuongTong/BangLuongTong'

export default function NSHomePage() {
    let menu = [{
        menu_id: 1,
        menu_name: "Chấm Công"
    },{
        menu_id: 2,
        menu_name: "Tạo Bảng Lương"
    },{
        menu_id: 3,
        menu_name: "Lương Tổng Hợp"
    },{
        menu_id: 4,
        menu_name: "BHXH"
    },{
        menu_id: 5,
        menu_name: "Ký Quỹ"
    },{
        menu_id: 6,
        menu_name: "Công Nợ"
    },{
        menu_id: 7,
        menu_name: "Đào Tạo"
    }]
    let [currentMenu,setCurrentMenu] = useState(1);
    let renderMenu = () => {
        return menu.map((item) => {
            return <li className={`${item.menu_id==currentMenu&&'active'}`} onClick={() => {setCurrentMenu(item.menu_id)}}>
                {item.menu_name}
            </li>
        });
    }
    let renderContent = () => {
        switch(currentMenu){
            case 1: return <ChamCong></ChamCong>
            case 2: return <TaoBangLuong></TaoBangLuong>
            case 3: return <BangLuongTong></BangLuongTong>
            default: return <ChamCong></ChamCong>
        }
    }
  return (
    <div id='NSHomePage'>
        <div className='NS_header bg-slate-100'>
            <ul>
                {renderMenu()}
            </ul>
        </div>
        <div className='NS_content'>
            {renderContent()}
        </div>
    </div>
  )
}
