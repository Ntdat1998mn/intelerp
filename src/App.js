import "./App.css";
import "./issets/css/homepage.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout/Layout";
import TrangChu from "./Pages/TrangChu/TrangChu";
import NSHomePage from "./Pages/NhanSu/NSHomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout Component={TrangChu} />}></Route>
        <Route path="/dang-nhap" element={<LoginPage />}></Route>
        <Route
          path="/nhan-su"
          element={<Layout Component={NSHomePage} />}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
