import { message } from "antd";

const showMessage = (content, type = "info") => {
  message[type](content);
};
const formatDate = (date) => {
  const daysOfWeek = [
    "Chủ Nhật",
    "Thứ 2",
    "Thứ 3",
    "Thứ 4",
    "Thứ 5",
    "Thứ 6",
    "Thứ 7",
  ];
  const months = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
  ];
  const dayOfWeek = daysOfWeek[date.getDay()];
  const day = date.getDate();
  const month = months[date.getMonth()];
  const year = date.getFullYear();

  return `${dayOfWeek} ngày ${day} tháng ${month} năm ${year}`;
};
const formatDateDDMMYYYY = (inputDate) => {
  if (!inputDate) {
    return;
  }
  const dateParts = inputDate.split("-");
  const day = dateParts[2];
  const month = dateParts[1];
  const year = dateParts[0];

  return `${day}/${month}/${year}`;
};

// Tìm ngày trong tuần gần nhất
const getDayOfNextWeek = () => {
  const today = new Date();
  const currentDay = today.getDay();

  let daysUntilNextMonday;
  if (currentDay === 1) {
    // Hôm nay đã là Thứ 2
    daysUntilNextMonday = 7;
  } else {
    // Tìm số ngày cần đến Thứ 2 tiếp theo
    daysUntilNextMonday = 8 - currentDay;
  }

  // Tính ngày Thứ 2 gần nhất
  const nextMonday = new Date(today);
  nextMonday.setDate(today.getDate() + daysUntilNextMonday);
  // Thêm ngày của cả tuần vào trong mảng
  const dayOfNextWeek = [];
  for (let i = 0; i < 7; i++) {
    const day = new Date(nextMonday);
    day.setDate(nextMonday.getDate() + i);
    dayOfNextWeek.push(day.toISOString().slice(0, 10));
  }
  return dayOfNextWeek;
};

export { showMessage, formatDate, formatDateDDMMYYYY, getDayOfNextWeek };
