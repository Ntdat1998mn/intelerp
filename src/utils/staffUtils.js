// kiểm tra loại bỏ các phần tử trùng nhau
export const getUniqueItems = (initialList, ...arraysNeedToRemoveList) => {
  const combinedNeedToRemoveArray = arraysNeedToRemoveList.reduce(
    (acc, arr) => acc.concat(arr),
    []
  );
  const idsInArraysNeedToRemoveList = {};
  combinedNeedToRemoveArray.forEach((item) => {
    idsInArraysNeedToRemoveList[item.id] = true;
  });
  const uniqueItems = initialList.filter(
    (item) => !idsInArraysNeedToRemoveList[item.id]
  );
  return uniqueItems;
};
