import {
  dropFolder,
  setActiveFolder,
  setEditingFolderId,
  setFolderForm,
} from "../reduxToolkit/reducer/folderSlice";
// Tìm đường dẫn của thư mục
export const findPathByFolderId = (folderList, targetId, paths = []) => {
  for (let folder of folderList) {
    const updatedPaths = [...paths, folder];
    if (folder.id === targetId) {
      return updatedPaths;
    } else if (folder.children?.length > 0) {
      const foundPaths = findPathByFolderId(
        folder.children,
        targetId,
        updatedPaths
      );
      if (foundPaths.length > 0) {
        return foundPaths;
      }
    }
  }
  return [];
};
export const findAndChangeNameByFolderId = (folderList, input, targetId) => {
  for (let folder of folderList) {
    if (folder.id === targetId) {
      folder.name = input;
    } else if (folder.children?.length > 0) {
      findAndChangeNameByFolderId(folder.children, input, targetId);
    }
  }
  return folderList;
};

// Sửa tên thư mục
export const handleEditName = (dispatch, folder, type) => {
  dispatch(setActiveFolder(folder));
  dispatch(setFolderForm({ name: folder.name }));
  dispatch(setEditingFolderId({ [type]: folder.id }));
};
// Xử lý sự kiện kéo thả thư mục
// Xử lý sự kiện khi bắt đầu kéo thư mục
export const handleDragStart = (e, folder) => {
  const data = {
    id: folder.id,
    name: folder.name,
  };
  e.dataTransfer.setData("application/json", JSON.stringify(data));
};

// Xử lý sự kiện khi thả thư mục
export const handleDrop = (e, folder, dispatch) => {
  e.preventDefault();
  // Lấy dữ liệu được kéo từ dataTransfer
  const droppedData = e.dataTransfer.getData("application/json");
  const droppedFolder = JSON.parse(droppedData);
  if (droppedFolder.id !== folder.id) {
    dispatch(
      dropFolder({
        droppedFolderId: droppedFolder.id,
        targetFolderId: folder.id,
      })
    );
  }
};

// Xử lý sự kiện khi kéo thư mục được hover trên vùng cho phép thả
export const handleDragOver = (e) => {
  e.preventDefault();
};
// Tìm kiếm thư mục theo id cắt và trả thư mục
export const findAndSpliceFolderByFolderId = (folderList, targetId) => {
  for (let i = 0; i < folderList.length; i++) {
    if (folderList[i].id === targetId) {
      return folderList.splice(i, 1)[0];
    } else if (folderList[i].children?.length > 0) {
      let spliceFolder = findAndSpliceFolderByFolderId(
        folderList[i].children,
        targetId
      );
      if (spliceFolder) return spliceFolder;
    }
  }
};

// Tìm kiếm thư mục được thả và thêm thư mục
export const findAndPushFolderByFolderId = (
  folderList,
  targetId,
  inputFolder
) => {
  for (let folder of folderList) {
    if (folder.id === targetId) {
      folder.children.push(inputFolder);
    } else if (folder.children?.length > 0) {
      findAndPushFolderByFolderId(folder.children, targetId, inputFolder);
    }
  }
  return folderList;
};
