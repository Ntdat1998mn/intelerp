module.exports = {
  apps: [
    {
      name: "qly_tailieu_3006",
      script: "serve", // Sử dụng tool serve để phục vụ tệp tĩnh
      env: {
        PM2_SERVE_PATH: "./build",
        PM2_SERVE_PORT: 3006, // Cổng HTTPS
        PM2_SERVE_SPA: "true",
      },
    },
  ],
};
